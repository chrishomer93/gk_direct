﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GkTiesBackend.DAL;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class PageSectionController : Controller
    {
        private Gk_TiesEntities1 db = new Gk_TiesEntities1();
        private IPageSectionService pageSectionService;

        public PageSectionController()
        {

        }

        public PageSectionController(IPageSectionService _pageSectionService)
        {
            this.pageSectionService = _pageSectionService;
        }


        // GET: PageSection
        public ActionResult Index()
        {
            return View(db.tbl_Page_Section.ToList());
        }

        // GET: PageSection/Details/5
        public ActionResult Details(decimal id)
        {
            tbl_Page_Section tbl_Page_Section = this.pageSectionService.detials(id);
            if (tbl_Page_Section == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Page_Section);
        }

        // GET: PageSection/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PageSection/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Description,Page,SectionName")] tbl_Page_Section tbl_Page_Section)
        {
            if (ModelState.IsValid)
            {
                tbl_Page_Section = this.pageSectionService.Create(tbl_Page_Section);
                return RedirectToAction("Index");
            }

            return View(tbl_Page_Section);
        }

        // GET: PageSection/Edit/5
        public ActionResult Edit(decimal id)
        {
            tbl_Page_Section tbl_Page_Section = this.pageSectionService.getEdit(id);
            if (tbl_Page_Section == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Page_Section);
        }

        // POST: PageSection/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,Page,SectionName")] tbl_Page_Section tbl_Page_Section)
        {
            if (ModelState.IsValid)
            {
                this.pageSectionService.postEdit(tbl_Page_Section);
                return RedirectToAction("Index");
            }
            return View(tbl_Page_Section);
        }

        // GET: PageSection/Delete/5
        public ActionResult Delete(decimal id)
        {
            tbl_Page_Section tbl_Page_Section = this.pageSectionService.getDelete(id);
            if (tbl_Page_Section == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Page_Section);
        }

        // POST: PageSection/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
