﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using BusinessEntityManager.Interfaces;
using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.ViewModels;
using System.Web;
using System.IO;
using System.Data;
using System.Data.Entity;

namespace BusinessEntityManager.Classes
{
    public class ColorSchemeService : IColorSchemeService
    {
        public tbl_ColorScheme createColorScheme(tbl_ColorScheme cs)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_ColorScheme tbl_ColorScheme = db.tbl_ColorScheme.Add(cs);
                db.SaveChanges();
                return tbl_ColorScheme;
            }
        }

        public tbl_ColorScheme details(decimal Id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_ColorScheme tbl_ColorScheme = db.tbl_ColorScheme.Find(Id);
                return tbl_ColorScheme;
            }
        }

        public tbl_ColorScheme editColorScheme(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_ColorScheme tbl_ColorScheme = db.tbl_ColorScheme.Find(id);
                return tbl_ColorScheme;
            }

        }

        public tbl_ColorScheme changeColorTheme(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_ColorScheme tbl_ColorScheme = db.tbl_ColorScheme.Find(id);
                //Find all color
                var findAllcolor = db.tbl_ColorScheme.ToList();
                foreach (var item in findAllcolor)
                {
                    if (item.Id != id)
                    {
                        item.Active = false;
                    }
                    else
                    {
                        item.Active = true;
                    }
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return tbl_ColorScheme;

            }
        }
        public tbl_ColorScheme findDeleteId(decimal Id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_ColorScheme tbl_ColorScheme = db.tbl_ColorScheme.Find(Id);
                return tbl_ColorScheme;
            }

        }

        public tbl_ColorScheme delete(decimal Id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_ColorScheme tbl_ColorScheme = db.tbl_ColorScheme.Find(Id);
                db.tbl_ColorScheme.Remove(tbl_ColorScheme);
                db.SaveChanges();
                return tbl_ColorScheme;
            }

        }
    }
}
