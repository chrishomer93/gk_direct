﻿using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CouponController : ApiController
    {
        private readonly ICouponService couponService;

        public CouponController(ICouponService _couponService)
        {
            this.couponService = _couponService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CheckCoupon(string Id)
        {

            var findCoupond = this.couponService.FindCoupon(Id);
            if (findCoupond != null)
            {
                if (findCoupond.fld_Expire != true)
                {
                    return Ok(new { status = "success", amount = findCoupond.fld_Amount, type = findCoupond.fld_Type, id = findCoupond.Id });
                }
                else
                {
                    return Ok(new { status = "Expire" });
                }
            }
            else
            {
                return Ok(new { status = "notFound" });
            }

        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CreateCoupon()
        {
            for (int i = 0; i < 5; i++)
            {
                var coupon = new tbl_Coupons();
                coupon.fld_Amount = "10";
                coupon.fld_CouponCode = "gk_" + RandomString(5);
                coupon.fld_Datetime = DateTime.Now;
                coupon.fld_Expire = false;
                coupon.fld_Type = "flat";
                var add = this.couponService.AddCoupon(coupon);
            }
            return Ok();

        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
