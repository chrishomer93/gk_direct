﻿using BusinessEntityManager.Interfaces;
using Database;
using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CategoryProuductController : ApiController
    {
        private readonly ICategoryProductService categoryProductService;

        public CategoryProuductController(ICategoryProductService _categoryProductService)
        {
            this.categoryProductService = _categoryProductService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IList<tbl_gk_Category> AllCategory()
        {
            var response = new CategoryRepository().GetAll().ToList();
            return response;
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Category(decimal Id)
        {
            return Ok(new CategoryRepository().FirstOrDefault(x => x.Id == Id));
        }
        [AllowAnonymous]
        [HttpGet]
        public IList<tbl_gk_Product> CategoryProduct(decimal Id)
        {
            var response = new ProductRepositroy().GetAll(x => x.fld_CategoryId == Id).ToList();
            return response;
        }

        public enum TieStyle
        {
            Business,
            Classic,
            Luxury,
            Wedding,
            Floral,
            Other,
            Paisley,
            Plain,
            PolkaDot,
            Striped,
            Normal,
            Skinny,
            Polyester,
            Silk,
            Wool
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CategoryCountDetailsProduct(decimal Id)
        {
            var AllProduct = this.categoryProductService.GetAllProductByCategory(Id);
            if (AllProduct.Count > 0)
            {
                //Style
                var findBusinessStyle = this.categoryProductService.FindCategoryCountDetailsProductStyle(TieStyle.Business.ToString(), Id);
                var findClassicStyle = this.categoryProductService.FindCategoryCountDetailsProductStyle(TieStyle.Classic.ToString(), Id);
                var findLuxuryStyle = this.categoryProductService.FindCategoryCountDetailsProductStyle(TieStyle.Luxury.ToString(), Id);
                var findWeddingStyle = this.categoryProductService.FindCategoryCountDetailsProductStyle(TieStyle.Wedding.ToString(), Id);
                //Pattern
                var findFloralPattern = this.categoryProductService.FindCategoryCountDetailsProductPattern(TieStyle.Floral.ToString(), Id);
                var findOtherPattern = this.categoryProductService.FindCategoryCountDetailsProductPattern(TieStyle.Other.ToString(), Id);
                var findPaisleyPattern = this.categoryProductService.FindCategoryCountDetailsProductPattern(TieStyle.Paisley.ToString(), Id);
                var findPlainPattern = this.categoryProductService.FindCategoryCountDetailsProductPattern(TieStyle.Plain.ToString(), Id);
                var findPolkaDotPattern = this.categoryProductService.FindCategoryCountDetailsProductPattern(TieStyle.PolkaDot.ToString(), Id);
                var findStripedPattern = this.categoryProductService.FindCategoryCountDetailsProductPattern(TieStyle.Striped.ToString(), Id);
                //Width
                var findNormalWidth = this.categoryProductService.FindCategoryCountDetailsProductWidth(TieStyle.Normal.ToString(), Id);
                var findSkinnyWidth = this.categoryProductService.FindCategoryCountDetailsProductWidth(TieStyle.Skinny.ToString(), Id);
                //Matrial
                var findMatrialPolyester = this.categoryProductService.FindCategoryCountDetailsProductMaterial(TieStyle.Polyester.ToString(), Id);
                var findMatrialSilk = this.categoryProductService.FindCategoryCountDetailsProductMaterial(TieStyle.Silk.ToString(), Id);
                var findMatrialWool = this.categoryProductService.FindCategoryCountDetailsProductMaterial(TieStyle.Wool.ToString(), Id);
                return Ok(new
                {
                    Business = findBusinessStyle,
                    Classic = findClassicStyle,
                    Luxury = findLuxuryStyle,
                    Wedding = findWeddingStyle,
                    Floral = findFloralPattern,
                    Other = findOtherPattern,
                    Paisley = findPaisleyPattern,
                    Plain = findPlainPattern,
                    PolkaDot = findPolkaDotPattern,
                    Striped = findStripedPattern,
                    Normal = findNormalWidth,
                    Skinny = findSkinnyWidth,
                    Polyester = findMatrialPolyester,
                    Silk = findMatrialSilk,
                    Wool = findMatrialWool,
                });
            }
            else
            {
                return Ok(new
                {
                    Business = 0,
                    Classic = 0,
                    Luxury = 0,
                    Wedding = 0,
                    Floral = 0,
                    Other = 0,
                    Paisley = 0,
                    Plain = 0,
                    PolkaDot = 0,
                    Striped = 0,
                    Normal = 0,
                    Skinny = 0,
                    Polyester = 0,
                    Silk = 0,
                    Wool = 0,
                });
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CategoryProductByColor(decimal Id, string color)
        {
            return Ok(this.categoryProductService.GetProductByColor(Id, color));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CategoryProductByStyle(decimal Id, string style)
        {
            return Ok(this.categoryProductService.GetProductByStyle(Id, style));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CategoryProductByPattern(decimal Id, string pattern)
        {
            return Ok(this.categoryProductService.GetProductByPattern(Id, pattern));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CategoryProductByWidth(decimal Id, string width)
        {
            return Ok(this.categoryProductService.GetProductByWidth(Id, width));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CategoryProductByMatrial(decimal Id, string matril)
        {
            return Ok(this.categoryProductService.GetProductByMaterial(Id, matril));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult CategoryProductByprice(decimal Id, decimal price)
        {
            return Ok(this.categoryProductService.GetProductByPrice(Id, price));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult AllProudct()
        {
            return Ok(this.categoryProductService.GetAllProduct());
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult AllNewArrivalProudct()
        {
            return Ok(this.categoryProductService.GetAllNewArrivalProduct());
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetAllNewArrival()
        {
            return Ok(this.categoryProductService.GetAllNewArrival());
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult ProductDetails(decimal Id)
        {
            return Ok(this.categoryProductService.GetProductDetail(Id));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult NewProductArrival(decimal Id)
        {
            return Ok(this.categoryProductService.GetNewProductArrival(Id));
        }
    }
}
