﻿$(document).ready(function () {
    loadProduct();
    LoadCategoryList();
});
var CreateProudct = function () {
    $("#Name").val("");
    $("#Description").val("");
    $("#Price").val("");
    $("#Size").val("");
    $("#Color").val("");
    $("#Tags").val("");
    $("#thumbnil").attr("src", "");
    $("#Brands").val("");
    $("#Quentity").val("");
    $("#Images").val("");
    $("#drpcategory").val("");
    $("#btnForm").prop('value', 'Create');
    $("#large").modal();
    $("#Id").val(0);
    clearRest();
};
var loadProduct = function () {
    var ur = $("#hidurlOfCategory").val();
    $.ajax({
        url: ur,
        type: "Get",
        success: function (res) {
            $("#loadProductTable").html(res);
            $("#tblProduct").DataTable();
        }
    });

};
var RegisterNewProduct = function () {
    var image = saveImage();
    if (typeof image !== "undefined") {
        var oldImage = $("#Images").val();
        console.log("Old" + oldImage);

        var iamge = "";
        for (var i = 0; i < image.length; i++) {
                if (i == 0) {
                    iamge = image[i];
                } else {
                    iamge = iamge + "," + image[i];
                }
        }
        $("#Images").val(oldImage + "," + iamge);
        //if (oldImage != "") {

        //    var oldimageArray = oldImage.split(",");
        //    for (var i = 0; i < oldimageArray.length; i++) {

        //    }
        //    $("#Images").val(image);
        //} else {
        //    $("#Images").val(image);
        //}
     
     
    }
    var category = $("#drpcategory").val();
    if (category !== "") {
        $("#CategoryId").val(category);
        var isFormValid = $(".form-horizontal").valid();
        if (isFormValid) {
            $.ajax({
                url: $("#myfrom").attr('action'),
                type: $("#myfrom").attr('method'),
                data: $("#myfrom").serialize(),
                success: function (res) {

                    if (res == "True") {
                        $.toast({
                            heading: 'Category Update',
                            text: 'Category save to database successfully',
                            icon: 'info',
                            loader: true,        // Change it to false to disable loader
                            loaderBg: '#9EC600',  // To change the background
                            position: 'bottom-right'
                        });
                        loadProduct();

                        $("#large").modal("hide");
                        clearRest();
                    }
                    else {
                        $.toast({
                            heading: 'Category Not Update',
                            text: 'Category  Not save to database successfully',
                            icon: 'error',
                            loader: true,        // Change it to false to disable loader
                            loaderBg: '#9EC600',  // To change the background
                            position: 'bottom-right'
                        });
                    }
                }

            });
        }
    } else {
        $.toast({
            heading: 'Category Not Select',
            text: 'Category  Not Select',
            icon: 'error',
            loader: true,        // Change it to false to disable loader
            loaderBg: '#9EC600',  // To change the background
            position: 'bottom-right'
        });
    }

};
var currentImage;
var UpdateProduct = function (id, name, description, price, size, color, style, pattern, quentity, category, image, matrial, itemCode, option) {
    $("#large").modal();
    $("#Id").val(id);
    $("#Name").val(name);
    $("#Description").val(description);
    $("#Price").val(price);
    $("#Quentity").val(quentity);
    $("#drpcategory").val(category);
    $("#Images").val(image);
    $("#ItemCode").val(itemCode);
    SelectColor(color);
    selectWidth(size);
    selectPattern(pattern);
    selectMatrial(matrial);
    selectStyle(style);
    selectOption(option);
    var imageList = image.split(',');
    var imgg = "";
    currentImage = imageList;
    if (imageList.length > 0) {
        for (var i = 0; i < imageList.length; i++) {
            imgg += `<div class="col-md-3" id="${i}" > <i onclick="removeImageOld(${i})" style="cursor:pointer" class="badge badge-danger fa fa-times-circle fx-removeButton"></i> <img class="img-thumbnail clear form-control" style="width:200px; height:200px; margin-top:10px;" src="Images/Product/${imageList[i]}" alt="Dimensions (220 X 220) pixels" /> </div>`;
        }

        $("#imagesFile").html(imgg);

        $("#thumbnil").attr("src", "Images/Product/" + image);

    };
    $("#btnForm").prop('value', 'Update');
};
var clearRest = function(){

    $("#Id").val("");
    $("#Name").val("");
    $("#Description").val("");
    $("#Price").val("");
    $("#Quentity").val("");
    $("#drpcategory").val("");
    $("#Images").val("");
    $("#ItemCode").val("");
    $("ul.color  li").removeClass("fx-active-select");
    $("ul.style  li").removeClass("fx-active-select");
    $("ul.pattern  li").removeClass("fx-active-select");
    $("ul.matrial  li").removeClass("fx-active-select");
    $("ul.width  li").removeClass("fx-active-select");
    $("ul.option  li").removeClass("fx-active-select");
    $("#Option").val("");
    $("#WIdth").val("");
    $("#Material").val("");
    $("#Style").val("");
    $("#Pattern").val("");
    $("#Color").val("");
    $("#imagesFile").html("");

    $("#btnForm").prop('value', 'Create');
}
 
var DeleteProudct = function (id) {
    var Geturl = $("#hidurlDeleteCategory").val();

    $.ajax({
        url: Geturl,
        type: "Post",
        data: { id: id },
        success: function (res) {
            if (res == "True") {
                $.toast({
                    heading: 'Category Delete',
                    text: 'Category Delete successfully',
                    icon: 'info',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
                loadProduct();
            }
            else {
                $.toast({
                    heading: 'Category Not Delete',
                    text: 'Category  Not Delete',
                    icon: 'error',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
            }
        }

    });


}
var saveImage = function () {
    var url = $("#hidurlImageUpload").val();
    var imageName;
    var formData = new FormData();
    var totalFiles = document.getElementById("ImgUpload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("ImgUpload").files[i];

        formData.append("ImgUpload", file);
    }
    if (totalFiles > 0) {
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function (res) {
                imageName = res;
            }
        });
    }
    return imageName;
};
var imageList = [];
function showMyImage(fileInput) {

    var files = fileInput.files;
    var imgg = "";
    for (var i = 0; i < files.length; i++) {
            var reader = new FileReader();

        reader.onload = function (e) {
            let id = e.timeStamp.toString().split(".");
            let obj = { id: id[0], image: e.target.result };
            imageList.push(obj);
            imgg = `<div class="col-md-3" id="${id[0]}" > <i onclick="removeImage(${id[0]})" style="cursor:pointer" class="badge badge-danger fa fa-times-circle fx-removeButton"></i> <img class="img-thumbnail clear form-control" style="width:200px; height:200px; margin-top:10px;" src="${e.target.result}" alt="Dimensions (220 X 220) pixels" /> </div>`;
            $("#imagesFile").append(imgg);
        };
            reader.readAsDataURL(files[i]);
        //var file = files[i];
        //var imageType = /image.*/;
        //if (!file.type.match(imageType)) {
        //    continue;
        //}
       
        ////var img = document.getElementById("thumbnil");

        ////img.file = file;
        //var reader = new FileReader();
        //reader.onload = (function (aImg) {
        //    return function (e) {
        //        let image = ` <img id="thumbnil${i}" class="img-thumbnail clear form-control" style="width:200px; height:200px; margin-top:10px;" src="${e.target.result}" alt="Dimensions (220 X 220) pixels" />`;
        //        $("#imagesFile").append(image);

        //    };
        //})(img);
        //reader.readAsDataURL(file);
    }
  
}
var LoadCategoryList = function () {
    var message = "";
    var getUrl = $("#hidurlLoadCategory").val();
    $.ajax({
        url: getUrl,
        type: "POST",
        success: function (msg) {
            if (msg.res === "true") {
                $("#drpcategory").html(msg.drpmarlasetting);

            }


        },
        error: function (res) {

        }
    });
};
var removeImage = function (id) {
    $("#" + id).remove();
};
var removeImageOld = function (index) {
    debugger
    $("#" + index).remove();
    currentImage.splice(index, 1);
    var img = "";
    if (currentImage.length > 0) {
        
     
        for (var i = 0; i < currentImage.length; i++) {
            if (i == 0) {
                img = currentImage[i];
            } else {
                img = img + "," + currentImage[i];
            }
          
        }
        
    }
    $("#Images").val(img);
};
var SelectColor = function (color) {
    $("ul.color  li").removeClass("fx-active-select");
    $("#" + color).addClass("fx-active-select");
    $("#Color").val(color);
}
var selectStyle = function (style) {
    $("ul.style  li").removeClass("fx-active-select");
    $("#" + style).addClass("fx-active-select");
    $("#Style").val(style);
}
var selectPattern = function (pattern) {
    $("ul.pattern  li").removeClass("fx-active-select");
    $("#" + pattern).addClass("fx-active-select");
    $("#Pattern").val(pattern);
}
var selectMatrial = function (matrial) {
    $("ul.matrial  li").removeClass("fx-active-select");
    $("#" + matrial).addClass("fx-active-select");
    $("#Material").val(matrial);
}
var selectWidth = function (width) {
    $("ul.width  li").removeClass("fx-active-select");
    $("#" + width).addClass("fx-active-select");
    $("#WIdth").val(width);
}
var selectOption = function (type) {
    $("ul.option  li").removeClass("fx-active-select");
    $("#" + type).addClass("fx-active-select");
    $("#Option").val(type);
}