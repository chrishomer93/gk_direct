﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using BusinessEntityManager.Interfaces;
using Database;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SocialMedaiLinksController : ApiController
    {
        private ISocialMediaLinkService socialMediaLinkService;

        public SocialMedaiLinksController(ISocialMediaLinkService _socialMediaLinkService)
        {
            this.socialMediaLinkService = _socialMediaLinkService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetSocialLnks()
        {
            return Ok(this.socialMediaLinkService.GetSocialMediaLinks());
        }
    }
}
