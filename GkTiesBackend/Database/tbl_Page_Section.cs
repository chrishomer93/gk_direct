//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Page_Section
    {
        public decimal Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Page { get; set; }
        public string SectionName { get; set; }
    }
}
