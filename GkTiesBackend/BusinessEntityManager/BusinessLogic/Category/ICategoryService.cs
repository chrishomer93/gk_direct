﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using GkTiesBackend.ViewModels;

namespace BusinessEntityManager.Interfaces
{
    public interface ICategoryService
    {
        bool saveCategory(CategoryViewModal cvm);
        bool deleteCategory(decimal Id);
        bool uploadCategory();
    }
}
