﻿using BusinessEntityManager.Interfaces;
using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;

namespace BusinessEntityManager.Classes
{
    public class AdminService : IAdminService
    {
        public tbl_gk_Admin LoginAdmin(string email, string password)
        {
            return new AdminRepository().FirstOrDefault(x => x.Email == email && x.Password == password);
        }
    }
}
