﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Interfaces
{
    public interface IWishListService
    {
        List<tbl_gk_WishList> GetWishListByUserId(decimal userId);
        List<tbl_gk_WishList> GetAllWishList();
        tbl_gk_WishList GetWishListbyId(decimal id);
        void AddWishList(tbl_gk_WishList wishList);
        void UpdateWishList(tbl_gk_WishList wishList);
        void DeleteWishList(tbl_gk_WishList wishList);
    }
}
