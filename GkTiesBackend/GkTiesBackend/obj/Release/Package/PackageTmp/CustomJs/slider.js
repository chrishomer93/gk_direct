﻿$(document).ready(function () {
    loadSlider();
});
var CreateSlider = function () {

    $("#btnForm").prop('value', 'Create');
    $("#large").modal();
    restValue();
};
var loadSlider = function () {
    var ur = $("#hidurlOfSlider").val();
    $.ajax({
        url: ur,
        type: "Get",
        success: function (res) {
            $("#loadSliderTable").html(res);
            $("#tblSlider").DataTable();
        }
    });

};
var RegisterNewSlider = function () {
    var image = saveImage();
    if (typeof image !== "undefined") {
        $("#Image").val(image);
    }
    var isFormValid = $(".form-horizontal").valid();
    if (isFormValid) {
        $.ajax({
            url: $("#myfrom").attr('action'),
            type: $("#myfrom").attr('method'),
            data: $("#myfrom").serialize(),
            success: function (res) {
                
                if (res == "True") {
                    $.toast({
                        heading: 'Slider Update',
                        text: 'Slider save to database successfully',
                        icon: 'info',
                        loader: true,        // Change it to false to disable loader
                        loaderBg: '#9EC600',  // To change the background
                        position: 'bottom-right'
                    });
                    loadSlider();
                    $("#large").modal("hide");
                    restValue();
                }
                else {
                    $.toast({
                        heading: 'Slider Not Update',
                        text: 'Slider  Not save to database successfully',
                        icon: 'error',
                        loader: true,        // Change it to false to disable loader
                        loaderBg: '#9EC600',  // To change the background
                        position: 'bottom-right'
                    });
                }
            }

        });
    }
};
var UpdateSlider = function (id, title,Priorty,image,status) {
    $("#large").modal();
    $("#Id").val(id);
    $("#Title").val(title);
    $("#Preiorty").val(Priorty);
    $("#Image").val(image);
    $("#Status").val(status);
    $("#thumbnil").attr("src", "Images/Slider/" + image);
    $("#btnForm").prop('value', 'Update');
};
var DeleteSlider = function (id) {
    var Geturl = $("#hidurlDeleteSlider").val();

    $.ajax({
        url: Geturl,
        type: "Post",
        data: { id: id },
        success: function (res) {
            if (res == "True") {
                $.toast({
                    heading: 'Slider Delete',
                    text: 'Slider Delete successfully',
                    icon: 'info',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
                loadSlider();
            }
            else {
                $.toast({
                    heading: 'Slider Not Delete',
                    text: 'Slider  Not Delete',
                    icon: 'error',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
            }
        }

    })


}
var saveImage = function () {
    var url = $("#hidurlImageUpload").val();
    var imageName;
    var formData = new FormData();
    var totalFiles = document.getElementById("ImgUpload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("ImgUpload").files[i];

        formData.append("ImgUpload", file);
    }
    if (totalFiles > 0) {
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function (res) {
                imageName = res;
            }
        });
    }
    return imageName;
};
function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {
            continue;
        }
        var img = document.getElementById("thumbnil");

        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
}

var restValue = function () {
    $("#Id").val("");
    $("#Title").val("");
    $("#Preiorty").val(0);
    $("#Image").val("");
    $("#thumbnil").attr("src","");
}

var UpdateStatus = function (id) {
    var Geturl = $("#hidurlChangeSlider").val();

    $.ajax({
        url: Geturl,
        type: "Post",
        data: { id: id },
        success: function (res) {
            if (res == "True") {
                $.toast({
                    heading: 'Slider Update',
                    text: 'Slider Update successfully',
                    icon: 'info',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
                loadSlider();
            }
            else {
                $.toast({
                    heading: 'Slider Not Update',
                    text: 'Slider  Not Update',
                    icon: 'error',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
            }
        }

    })


}