﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GkTiesBackend.ViewModels
{
    public class CategoryViewModal
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public string Images { get; set; }
        public decimal Id { get; set; }

    }
}