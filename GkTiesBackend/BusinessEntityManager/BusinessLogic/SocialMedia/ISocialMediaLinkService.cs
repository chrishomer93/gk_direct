﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Interfaces
{
    public interface ISocialMediaLinkService
    {
        tbl_SocialMediaLinks GetSocialMediaLinks();
        List<tbl_SocialMediaLinks> GetAllSocialMediaLinks();
        tbl_SocialMediaLinks FindSocialMediaLink(decimal socialMediaId);
        void AddSocialMediaLink(tbl_SocialMediaLinks socialMediaLink);
        void EditSocialMediaLink(tbl_SocialMediaLinks socialMediaLink);
        void DeleteSocialMediaLink(tbl_SocialMediaLinks socialMediaLink);
    }
}
