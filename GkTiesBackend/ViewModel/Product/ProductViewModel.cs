﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GkTiesBackend.ViewModels
{
    public class ProductViewModel
    {
        public decimal Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public string WIdth { get; set; }
        [Required]
        public string Color { get; set; }
        [Required]
        public string Style { get; set; }
        [Required]
        public string Pattern { get; set; }
        [Required]
        public string Images { get; set; }
        [Required]
        public string Material { get; set; }
        [Required]
        public string ItemCode { get; set; }
        [Required]
        public decimal Quentity { get; set; }
        [Required]
        public decimal CategoryId { get; set; }
        [Required]
        public string Option { get; set; }

    }
}