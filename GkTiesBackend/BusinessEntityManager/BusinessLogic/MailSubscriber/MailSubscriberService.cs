﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntityManager.Interfaces;
using Database;

namespace BusinessEntityManager.Classes
{
    public class MailSubscriberService : IMailSubscriberService
    {
        public tbl_MailSubscriber Create(tbl_MailSubscriber tbl_MailSubscriber)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.tbl_MailSubscriber.Add(tbl_MailSubscriber);
                db.SaveChanges();
                return tbl_MailSubscriber;
            }
        }

        public tbl_MailSubscriber delete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_MailSubscriber tbl_MailSubscriber = db.tbl_MailSubscriber.Find(id);
                db.tbl_MailSubscriber.Remove(tbl_MailSubscriber);
                db.SaveChanges();
                return tbl_MailSubscriber;
            }
        }

        public tbl_MailSubscriber details(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_MailSubscriber tbl_MailSubscriber = db.tbl_MailSubscriber.Find(id);
                return tbl_MailSubscriber;
            }
        }

        public tbl_MailSubscriber getDelete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_MailSubscriber tbl_MailSubscriber = db.tbl_MailSubscriber.Find(id);
                return tbl_MailSubscriber;
            }
        }

        public tbl_MailSubscriber getEdit(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_MailSubscriber tbl_MailSubscriber = db.tbl_MailSubscriber.Find(id);
                return tbl_MailSubscriber;
            }
        }

        public tbl_MailSubscriber postEdit(tbl_MailSubscriber tbl_MailSubscriber)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.Entry(tbl_MailSubscriber).State = EntityState.Modified;
                db.SaveChanges();
                return tbl_MailSubscriber;
            }
        }
    }
}
