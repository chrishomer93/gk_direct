﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;

namespace BusinessEntityManager.Interfaces
{
 public   interface IColorSchemeService
    {
        tbl_ColorScheme details(decimal Id);

        tbl_ColorScheme createColorScheme(tbl_ColorScheme cs);

        tbl_ColorScheme editColorScheme(decimal id);

        tbl_ColorScheme changeColorTheme(decimal id);
        tbl_ColorScheme findDeleteId(decimal Id);
        tbl_ColorScheme delete(decimal Id);

        
    }
}
