﻿using BusinessEntityManager.BusinessLogic.Review;
using BusinessEntityManager.Classes;
using BusinessEntityManager.Interfaces;
using GkTiesBackend.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Unity;

namespace GkTiesBackend
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            AddDependencyInjection(config);

            // Web API configuration and services
            config.Filters.Add(new AuthorizeAttribute());
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void AddDependencyInjection(HttpConfiguration configuration)
        {
            var container = new UnityContainer();
            container.RegisterType<IAuthorizationService, AuthorizationService>();
            container.RegisterType<IBookingPaymentService, BookingPaymentService>();
            container.RegisterType<ICategoryProductService, CategoryProductService>();
            container.RegisterType<ICouponService, CouponService>();
            container.RegisterType<IPageSectionService, PageSectionService>();
            container.RegisterType<ISliderService, SliderService>();
            container.RegisterType<ISocialMediaLinkService, SocialMediaLinkService>();
            container.RegisterType<IWishListService, WishListService>();
            container.RegisterType<IReviewService, ReviewService>();
            configuration.DependencyResolver = new UnityResolver(container);
        }
    }
}
