﻿using BusinessEntityManager.Interfaces;
using Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Classes
{
    public class PageSectionService : IPageSectionService
    {
        public tbl_Page_Section Create(tbl_Page_Section tbl_Page_Section)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.tbl_Page_Section.Add(tbl_Page_Section);
                db.SaveChanges();
                return tbl_Page_Section;
            }
        }

        public tbl_Page_Section Delete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_Page_Section tbl_Page_Section = db.tbl_Page_Section.Find(id);
                db.tbl_Page_Section.Remove(tbl_Page_Section);
                db.SaveChanges();
                return tbl_Page_Section;
            }
        }

        public tbl_Page_Section detials(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                    tbl_Page_Section tbl_Page_Section = db.tbl_Page_Section.Find(id);
                    return tbl_Page_Section;               
            }
        }

        public tbl_Page_Section getDelete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_Page_Section tbl_Page_Section = db.tbl_Page_Section.Find(id);
                return tbl_Page_Section;
            }
        }

        public tbl_Page_Section getEdit(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_Page_Section tbl_Page_Section = db.tbl_Page_Section.Find(id);
                return tbl_Page_Section;
            }
        }

        public tbl_Page_Section GetPageSection(string sectionName)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                return db.tbl_Page_Section.FirstOrDefault(x => x.SectionName == sectionName);
            }
        }

        public tbl_Page_Section postEdit(tbl_Page_Section tbl_Page_Section)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.Entry(tbl_Page_Section).State = EntityState.Modified;
                db.SaveChanges();
                return tbl_Page_Section;
            }
        }


    }
}
