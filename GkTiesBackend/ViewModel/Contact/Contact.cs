﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GkTiesBackend.ViewModels
{
    public class Contact
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}