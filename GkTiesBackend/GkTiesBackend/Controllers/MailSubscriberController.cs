﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GkTiesBackend.DAL;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class MailSubscriberController : Controller
    {
        public MailSubscriberController()
        {

        }
        public MailSubscriberController(IMailSubscriberService _mailSubscriberService)
        {
            this.mailSubscriberService = _mailSubscriberService;
        }

        private IMailSubscriberService mailSubscriberService;
        private Gk_TiesEntities1 db = new Gk_TiesEntities1();

        // GET: MailSubscriber
        public ActionResult Index()
        {
            return View(db.tbl_MailSubscriber.ToList());
        }

        // GET: MailSubscriber/Details/5
        public ActionResult Details(decimal id)
        {

            tbl_MailSubscriber tbl_MailSubscriber = this.mailSubscriberService.details(id);
            if (tbl_MailSubscriber == null)
            {
                return HttpNotFound();
            }
            return View(tbl_MailSubscriber);
        }

        // GET: MailSubscriber/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MailSubscriber/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Mail,CreateAt,Status")] tbl_MailSubscriber tbl_MailSubscriber)
        {
            if (ModelState.IsValid)
            {
                tbl_MailSubscriber mail= this.mailSubscriberService.Create(tbl_MailSubscriber);
                return RedirectToAction("Index");
            }

            return View(tbl_MailSubscriber);
        }

        // GET: MailSubscriber/Edit/5
        public ActionResult Edit(decimal id)
        {

            tbl_MailSubscriber tbl_MailSubscriber = this.mailSubscriberService.getEdit(id);
            if (tbl_MailSubscriber == null)
            {
                return HttpNotFound();
            }
            return View(tbl_MailSubscriber);
        }

        // POST: MailSubscriber/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Mail,CreateAt,Status")] tbl_MailSubscriber tbl_MailSubscriber)
        {
            if (ModelState.IsValid)
            {
                this.mailSubscriberService.postEdit(tbl_MailSubscriber);
                return RedirectToAction("Index");
            }
            return View(tbl_MailSubscriber);
        }

        // GET: MailSubscriber/Delete/5
        public ActionResult Delete(decimal id)
        {
            tbl_MailSubscriber tbl_MailSubscriber = this.mailSubscriberService.getDelete(id);
            if (tbl_MailSubscriber == null)
            {
                return HttpNotFound();
            }
            return View(tbl_MailSubscriber);
        }

        // POST: MailSubscriber/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
