﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GkTiesBackend.DAL;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class SocialMediaLinksController : Controller
    {
        private readonly ISocialMediaLinkService socialMediaLinkService;

        public SocialMediaLinksController()
        {

        }
        public SocialMediaLinksController(ISocialMediaLinkService _socialMediaLinkService)
        {
            this.socialMediaLinkService = _socialMediaLinkService;
        }
        private Gk_TiesEntities1 db = new Gk_TiesEntities1();

        // GET: SocialMediaLinks
        public ActionResult Index()
        {
            return View(this.socialMediaLinkService.GetAllSocialMediaLinks());
        }

        // GET: SocialMediaLinks/Details/5
        public ActionResult Details(decimal id)
        {
            tbl_SocialMediaLinks tbl_SocialMediaLinks = this.socialMediaLinkService.FindSocialMediaLink(id);
            if (tbl_SocialMediaLinks == null)
            {
                return HttpNotFound();
            }
            return View(tbl_SocialMediaLinks);
        }

        // GET: SocialMediaLinks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SocialMediaLinks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Facebook,Instagram,Twitter")] tbl_SocialMediaLinks tbl_SocialMediaLinks)
        {
            if (ModelState.IsValid)
            {
                this.socialMediaLinkService.AddSocialMediaLink(tbl_SocialMediaLinks);
                return RedirectToAction("Index");
            }

            return View(tbl_SocialMediaLinks);
        }

        // GET: SocialMediaLinks/Edit/5
        public ActionResult Edit(decimal id)
        {
            tbl_SocialMediaLinks tbl_SocialMediaLinks = this.socialMediaLinkService.FindSocialMediaLink(id);
            if (tbl_SocialMediaLinks == null)
            {
                return HttpNotFound();
            }
            return View(tbl_SocialMediaLinks);
        }

        // POST: SocialMediaLinks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Facebook,Instagram,Twitter")] tbl_SocialMediaLinks tbl_SocialMediaLinks)
        {
            if (ModelState.IsValid)
            {
                this.socialMediaLinkService.EditSocialMediaLink(tbl_SocialMediaLinks);
                return RedirectToAction("Index");
            }
            return View(tbl_SocialMediaLinks);
        }

        // GET: SocialMediaLinks/Delete/5
        public ActionResult Delete(decimal id)
        {
            tbl_SocialMediaLinks tbl_SocialMediaLinks = this.socialMediaLinkService.FindSocialMediaLink(id);
            if (tbl_SocialMediaLinks == null)
            {
                return HttpNotFound();
            }
            return View(tbl_SocialMediaLinks);
        }

        // POST: SocialMediaLinks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            tbl_SocialMediaLinks tbl_SocialMediaLinks = this.socialMediaLinkService.FindSocialMediaLink(id);
            this.socialMediaLinkService.DeleteSocialMediaLink(tbl_SocialMediaLinks);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
