﻿using GkTiesBackend.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;

namespace BusinessEntityManager.Interfaces
{
    public interface IAdminService
    {
        tbl_gk_Admin LoginAdmin(string email, string password);
    }
}
