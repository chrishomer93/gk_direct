﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;

namespace BusinessEntityManager.Interfaces
{
   public interface IMailSubscriberService
    {
        tbl_MailSubscriber details(decimal id);
        tbl_MailSubscriber Create(tbl_MailSubscriber tbl_MailSubscriber);
        tbl_MailSubscriber getEdit(decimal id);
        tbl_MailSubscriber postEdit(tbl_MailSubscriber tbl_MailSubscriber);
        tbl_MailSubscriber getDelete(decimal id);
        tbl_MailSubscriber delete(decimal id);
    }
}
