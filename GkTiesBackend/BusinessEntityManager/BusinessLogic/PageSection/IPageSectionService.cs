﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Interfaces
{
    public interface IPageSectionService
    {
        tbl_Page_Section GetPageSection(string sectionName);
        tbl_Page_Section detials(decimal id);
        tbl_Page_Section Create(tbl_Page_Section tbl_Page_Section);
        tbl_Page_Section getEdit(decimal id);
        tbl_Page_Section postEdit(tbl_Page_Section tbl_Page_Section);
        tbl_Page_Section getDelete(decimal id);
        tbl_Page_Section Delete(decimal id);
    }
}
