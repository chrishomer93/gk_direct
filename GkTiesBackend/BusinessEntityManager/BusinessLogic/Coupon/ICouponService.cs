﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Interfaces
{
    public interface ICouponService
    {
        bool AddCoupon(tbl_Coupons coupon);
        tbl_Coupons FindCoupon(string couponId);
    }
}
