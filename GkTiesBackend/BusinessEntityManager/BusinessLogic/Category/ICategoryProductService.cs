﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Interfaces
{
    public interface ICategoryProductService
    {
        List<tbl_gk_Product> GetNewProductArrival(decimal id);
        List<tbl_gk_Product> GetAllNewArrival();
        List<tbl_gk_Product> GetAllNewArrivalProduct();
        List<tbl_gk_Product> GetProductByPrice(decimal id, decimal price);
        List<tbl_gk_Product> GetProductByMaterial(decimal id, string material);
        List<tbl_gk_Product> GetProductByWidth(decimal id, string width);
        List<tbl_gk_Product> GetProductByPattern(decimal id, string pattern);
        List<tbl_gk_Product> GetProductByStyle(decimal id, string style);
        List<tbl_gk_Product> GetProductByColor(decimal id, string color);
        List<tbl_gk_Product> GetAllProduct();
        tbl_gk_Product GetProductDetail(decimal id);
        int FindCategoryCountDetailsProductStyle(string tieStyle, decimal catId);
        int FindCategoryCountDetailsProductPattern(string pattern, decimal catId);
        int FindCategoryCountDetailsProductWidth(string width, decimal catId);
        int FindCategoryCountDetailsProductMaterial(string material, decimal catId);
        List<tbl_gk_Product> GetAllProductByCategory(decimal catId);
    }
}
