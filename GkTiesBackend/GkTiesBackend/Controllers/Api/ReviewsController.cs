﻿using BusinessEntityManager.BusinessLogic.Review;
using System;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReviewsController : ApiController
    {
        private IReviewService _reviewService;
        public ReviewsController(IReviewService reviewService)
        {
            this._reviewService = reviewService;
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult AddReview(Database.tbl_gk_reviews review)
        {
            try
            {
                return Ok(this._reviewService.AddReview(review));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetAllReviews()
        {
            return Ok(this._reviewService.GetReviews());
        }

        //[HttpPost]
        //[AllowAnonymous]
        //public IHttpActionResult ContactUsEmail()
        //{

        //}
    }
}