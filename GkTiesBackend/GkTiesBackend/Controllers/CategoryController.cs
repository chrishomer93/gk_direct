﻿using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Database;
using BusinessEntityManager.Interfaces;


namespace GkTiesBackend.Controllers
{
    public class CategoryController : Controller
    {
        public CategoryController()
        {

        }
        public CategoryController(ICategoryService _categoryService)
        {
            this.categoryService = _categoryService;
        }

        private ICategoryService categoryService;
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult _LoadCategory()
        {
            var AllCategoryList = new CategoryRepository().GetAll().ToList();
            return PartialView(AllCategoryList);
        }
        public ActionResult CreateView()
        {
            return View();
        }
        [HttpPost]
        public bool SaveNewCategory(CategoryViewModal cvm)
        {
            if (ModelState.IsValid)
            {
                var saveCategory = this.categoryService.saveCategory(cvm);
                if (saveCategory)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        [HttpPost]
        public bool DeleteCategory(decimal Id)
        {
            var FindCategory = this.categoryService.deleteCategory(Id);
            if (ModelState.IsValid)
            {
                if (FindCategory)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public ActionResult Upload()
        {
            string path = string.Empty;
            List<string> filePath = new List<string>();
            if (Request.Files != null)
                try
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];

                        var fileName = Path.GetFileName(file.FileName);
                        var name = fileName.Split('.')[0];
                        var extension = fileName.Split('.')[1];
                        Random rand = new Random();
                        var r = rand.Next(1, 99999);


                        name = name + r;
                        fileName = name + "." + extension;
                        path = Path.Combine(Server.MapPath("/Images/Category"), fileName);
                        filePath.Add(fileName);
                        file.SaveAs(path);

                    }
                }
                catch (Exception)
                {

                }
            else
            {
                return Json("false");
            }
            return Json(filePath);

        }
    }
}