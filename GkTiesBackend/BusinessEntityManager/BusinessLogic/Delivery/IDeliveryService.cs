﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;

namespace BusinessEntityManager.Interfaces
{
   public interface IDeliveryService
    {
        tbl_Delivery_Package detials(decimal id);
        tbl_Delivery_Package edit(decimal id);
        tbl_Delivery_Package postEdit(tbl_Delivery_Package pkg);
        tbl_Delivery_Package delete(decimal id);
        tbl_Delivery_Package findDelete(decimal id);
        tbl_Delivery_Package create(tbl_Delivery_Package pkg);
    }
}
