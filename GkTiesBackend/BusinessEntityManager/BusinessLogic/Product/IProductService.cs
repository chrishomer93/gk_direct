﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using GkTiesBackend.ViewModels;

namespace BusinessEntityManager.Interfaces
{
    public interface IProductService
    {
        tbl_gk_Product LoadProductById(decimal id);
        bool SaveNewProduct(tbl_gk_Product cvm);
        tbl_gk_Product DeleteProduct(decimal Id);
        List<tbl_gk_Category> LoadCategory();
        bool UpdateProduct(tbl_gk_Product product);
    }
}
