﻿$(document).ready(function () {
    loadCategroy();
});
var CreateCategory = function () {
    $("#Name").val("");
    $("#Description").val("");
    $("#Images").val("");
    $("#btnForm").prop('value', 'Create');
    $("#large").modal();
    $("#Id").val(0);
};
var loadCategroy = function () {
    var ur = $("#hidurlOfCategory").val();
    $.ajax({
        url: ur,
        type: "Get",
        success: function (res) {
            $("#loadCategoryTable").html(res);
            $("#tblCategory").DataTable();
        }
    });

};
var RegisterNewCategory = function () {
    var image = saveImage();
    if (typeof image !== "undefined") {
        $("#Images").val(image);
    }
   
    var isFormValid = $(".form-horizontal").valid();
    if (isFormValid) {
        $.ajax({
            url: $("#myfrom").attr('action'),
            type: $("#myfrom").attr('method'),
            data: $("#myfrom").serialize(),
            success: function (res) {
                
                if (res == "True") {
                    $.toast({
                        heading: 'Category Update',
                        text: 'Category save to database successfully',
                        icon: 'info',
                        loader: true,        // Change it to false to disable loader
                        loaderBg: '#9EC600',  // To change the background
                        position: 'bottom-right'
                    });
                    loadCategroy();
                    $("#large").modal("hide");
                }
                else {
                    $.toast({
                        heading: 'Category Not Update',
                        text: 'Category  Not save to database successfully',
                        icon: 'error',
                        loader: true,        // Change it to false to disable loader
                        loaderBg: '#9EC600',  // To change the background
                        position: 'bottom-right'
                    });
                }
            }

        });
    }
};
var UpdateCategory = function (id, name, description, image) {
    $("#large").modal();
    $("#Id").val(id);
    $("#Name").val(name);
    $("#Description").val(description);
    $("#Images").val(image);
    $("#thumbnil").attr("src", "Images/Category/" + image);
    $("#btnForm").prop('value', 'Update');
};
var DeleteCategory = function (id) {
    var Geturl = $("#hidurlDeleteCategory").val();

    $.ajax({
        url: Geturl,
        type: "Post",
        data: { id: id },
        success: function (res) {
            if (res == "True") {
                $.toast({
                    heading: 'Category Delete',
                    text: 'Category Delete successfully',
                    icon: 'info',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
                loadCategroy();
            }
            else {
                $.toast({
                    heading: 'Category Not Delete',
                    text: 'Category  Not Delete',
                    icon: 'error',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
            }
        }

    })


}
var saveImage = function () {
    var url = $("#hidurlImageUpload").val();
    var imageName;
    var formData = new FormData();
    var totalFiles = document.getElementById("ImgUpload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("ImgUpload").files[i];

        formData.append("ImgUpload", file);
    }
    if (totalFiles > 0) {
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function (res) {
                imageName = res;
            }
        });
    }
    return imageName;
};
function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {
            continue;
        }
        var img = document.getElementById("thumbnil");

        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
}