﻿using BusinessEntityManager.Interfaces;
using CryptHelper;
using Database;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Classes
{
    public class AuthorizationService : IAuthorizationService
    {
        public bool AddUser(tbl_gk_User user)
        {
            return new CustomerRepository().Add(user);
        }

        public tbl_gk_User FindUser(string email, string password = "")
        {
            if (!string.IsNullOrEmpty(password))
            {
                var encodedPassword = PasswordHelper.EncodePasswordToBase64(password);
                return new CustomerRepository().FirstOrDefault(x => x.fld_Email.ToLower() == email && x.fld_Password == encodedPassword);
            }
            else
            {
                return new CustomerRepository().FirstOrDefault(x => x.fld_Email.ToLower() == email);
            }
        }
    }
}
