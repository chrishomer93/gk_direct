﻿using BusinessEntityManager.Interfaces;
using Database;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Classes
{
    public class SocialMediaLinkService : ISocialMediaLinkService
    {
        public void AddSocialMediaLink(tbl_SocialMediaLinks socialMediaLink)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.tbl_SocialMediaLinks.Add(socialMediaLink);
                db.SaveChanges();
            }
        }

        public void DeleteSocialMediaLink(tbl_SocialMediaLinks socialMediaLink)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.tbl_SocialMediaLinks.Remove(socialMediaLink);
                db.SaveChanges();
            }
        }

        public void EditSocialMediaLink(tbl_SocialMediaLinks socialMediaLink)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.Entry(socialMediaLink).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public tbl_SocialMediaLinks FindSocialMediaLink(decimal socialMediaId)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                return db.tbl_SocialMediaLinks.Find(socialMediaId);
            }
        }

        public List<tbl_SocialMediaLinks> GetAllSocialMediaLinks()
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                return db.tbl_SocialMediaLinks.ToList();
            }

        }
        public tbl_SocialMediaLinks GetSocialMediaLinks()
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                return db.tbl_SocialMediaLinks.FirstOrDefault();
            }
        }
    }
}
