﻿using BusinessEntityManager.Interfaces;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SliderController : ApiController
    {
        private readonly ISliderService sliderService;

        public SliderController(ISliderService _sliderService)
        {
            this.sliderService = _sliderService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult AllSlider()
        {

            return Ok(this.sliderService.GetSlider());
        }
    }
}
