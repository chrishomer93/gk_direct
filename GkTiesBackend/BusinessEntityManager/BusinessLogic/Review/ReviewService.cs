﻿using Database;
using DatabaseRepositories.Reviews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.BusinessLogic.Review
{
    public class ReviewService : IReviewService
    {
        public bool AddReview(tbl_gk_reviews review)
        {
            return new ReviewsRepository().Add(review);
        }

        public List<tbl_gk_reviews> GetReviews()
        {
            return new ReviewsRepository().GetAll().OrderBy(x => x.productId).ToList();
        }
    }
}
