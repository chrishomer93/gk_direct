﻿using BusinessEntityManager.Interfaces;
using GkTiesBackend.DAL.Repository;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WishlistController : ApiController
    {
        private IWishListService wishListService;

        public WishlistController(IWishListService _wishListService)
        {
            this.wishListService = _wishListService;
        }

        // GET api/<controller>/5
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Get(decimal Id)
        {
            var response = Ok(this.wishListService.GetWishListByUserId(Id));
            return response;
        }


        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}