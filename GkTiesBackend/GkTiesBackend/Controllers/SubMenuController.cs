﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GkTiesBackend.DAL;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class SubMenuController : Controller
    {
        private Gk_TiesEntities1 db = new Gk_TiesEntities1();
        public SubMenuController()
        {
        }
        public SubMenuController(ISubMenuService _subMenuService)
        {
            this.SubMenuService = _subMenuService;
        }

        private ISubMenuService SubMenuService;

        // GET: SubMenu
        public ActionResult Index()
        {
            var tbl_SubMenu = db.tbl_SubMenu.Include(t => t.tbl_parent_menu);
            return View(tbl_SubMenu.ToList());
        }

        // GET: SubMenu/Details/5
        public ActionResult Details(decimal id)
        {
            tbl_SubMenu tbl_SubMenu = this.SubMenuService.Details(id);
            if (tbl_SubMenu == null)
            {
                return HttpNotFound();
            }
            return View(tbl_SubMenu);
        }

        // GET: SubMenu/Create
        public ActionResult Create()
        {
            ViewBag.ParenetMenuId = new SelectList(db.tbl_parent_menu, "Id", "MenuName");
            return View();
        }

        // POST: SubMenu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SubMenuName,Url,ParenetMenuId")] tbl_SubMenu tbl_SubMenu)
        {
            if (ModelState.IsValid)
            {
                tbl_SubMenu = this.SubMenuService.Create(tbl_SubMenu);
                return RedirectToAction("Index");
            }

            ViewBag.ParenetMenuId = new SelectList(db.tbl_parent_menu, "Id", "MenuName", tbl_SubMenu.ParenetMenuId);
            return View(tbl_SubMenu);
        }

        // GET: SubMenu/Edit/5
        public ActionResult Edit(decimal id)
        {
            tbl_SubMenu tbl_SubMenu = this.SubMenuService.getEdit(id);
            if (tbl_SubMenu == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParenetMenuId = new SelectList(db.tbl_parent_menu, "Id", "MenuName", tbl_SubMenu.ParenetMenuId);
            return View(tbl_SubMenu);
        }

        // POST: SubMenu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SubMenuName,Url,ParenetMenuId")] tbl_SubMenu tbl_SubMenu)
        {
            if (ModelState.IsValid)
            {
                this.SubMenuService.postEdit(tbl_SubMenu);
                return RedirectToAction("Index");
            }
            ViewBag.ParenetMenuId = new SelectList(db.tbl_parent_menu, "Id", "MenuName", tbl_SubMenu.ParenetMenuId);
            return View(tbl_SubMenu);
        }

        // GET: SubMenu/Delete/5
        public ActionResult Delete(decimal id)
        {
            tbl_SubMenu tbl_SubMenu = this.SubMenuService.Delete(id);
            if (tbl_SubMenu == null)
            {
                return HttpNotFound();
            }
            return View(tbl_SubMenu);
        }

        // POST: SubMenu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
