﻿using BusinessEntityManager.Interfaces;
using Database;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace BusinessEntityManager.Classes
{
    public class WishListService : IWishListService
    {
        public void AddWishList(tbl_gk_WishList wishList)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.tbl_gk_WishList.Add(wishList);
                db.SaveChanges();
            }
        }

        public void DeleteWishList(tbl_gk_WishList wishList)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.Entry(wishList).State = EntityState.Deleted;
                db.SaveChanges();
            }
        }

        public List<tbl_gk_WishList> GetAllWishList()
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                return db.tbl_gk_WishList.Include(t => t.tbl_gk_Product).Include(t => t.tbl_gk_User).ToList();
            }
        }

        public tbl_gk_WishList GetWishListbyId(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                return db.tbl_gk_WishList.Include(t => t.tbl_gk_Product).Include(t => t.tbl_gk_User).Where(x => x.Id == id).FirstOrDefault();
            }
        }

        public List<tbl_gk_WishList> GetWishListByUserId(decimal userId)
        {
            return new WishlistRepository().GetAll(x => x.fld_User_Id == userId);
        }

        public void UpdateWishList(tbl_gk_WishList wishList)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.Entry(wishList).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
