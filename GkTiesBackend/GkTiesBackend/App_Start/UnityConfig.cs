using BusinessEntityManager.Classes;
using BusinessEntityManager.Interfaces;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace GkTiesBackend
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            container.RegisterType<IAdminService, AdminService>();
            container.RegisterType<ICategoryService, categoryService>();
            container.RegisterType<IAuthorizationService, AuthorizationService>();
            container.RegisterType<IColorSchemeService, ColorSchemeService>();
            container.RegisterType<ICustomerService, CustomerService>();
            container.RegisterType<IDeliveryService, DeliveryService>();
            container.RegisterType<IMailSubscriberService, MailSubscriberService>();
            container.RegisterType<IPageSectionService, PageSectionService>();
            container.RegisterType<IParentMenuService, ParentMenuService>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<ISubMenuService, SubMenuService>();
            container.RegisterType<ISliderService, SliderService>();
            container.RegisterType<ISocialMediaLinkService, SocialMediaLinkService>();
            container.RegisterType<IWishListService, WishListService>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }



    }
}