﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GkTiesBackend.DAL;
using Database;
using BusinessEntityManager.Interfaces;
using BusinessEntityManager.Classes;

namespace GkTiesBackend.Controllers
{
    public class ParentMenuController : Controller
    {
        private Gk_TiesEntities1 db = new Gk_TiesEntities1();

        public ParentMenuController()
        {

        }
        public ParentMenuController(IParentMenuService _parentMenuService)
        {
            this.ParentMenuService = _parentMenuService;
        }
        private IParentMenuService ParentMenuService;

        // GET: ParentMenu
        public ActionResult Index()
        {
            return View(db.tbl_parent_menu.ToList());
        }

        // GET: ParentMenu/Details/5
        public ActionResult Details(decimal id)
        {
            tbl_parent_menu tbl_parent_menu = this.ParentMenuService.Delete(id);
            if (tbl_parent_menu == null)
            {
                return HttpNotFound();
            }
            return View(tbl_parent_menu);
        }

        // GET: ParentMenu/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ParentMenu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MenuName,UrlLinks")] tbl_parent_menu tbl_parent_menu)
        {
            if (ModelState.IsValid)
            {
                tbl_parent_menu = this.ParentMenuService.Create(tbl_parent_menu);
                return RedirectToAction("Index");
            }

            return View(tbl_parent_menu);
        }

        // GET: ParentMenu/Edit/5
        public ActionResult Edit(decimal id)
        {
            tbl_parent_menu tbl_parent_menu = this.ParentMenuService.getEdit(id);
            if (tbl_parent_menu == null)
            {
                return HttpNotFound();
            }
            return View(tbl_parent_menu);
        }

        // POST: ParentMenu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MenuName,UrlLinks")] tbl_parent_menu tbl_parent_menu)
        {
            if (ModelState.IsValid)
            {
                tbl_parent_menu = this.ParentMenuService.postEdit(tbl_parent_menu);
                return RedirectToAction("Index");
            }
            return View(tbl_parent_menu);
        }

        // GET: ParentMenu/Delete/5
        public ActionResult Delete(decimal id)
        {
            tbl_parent_menu tbl_parent_menu = this.ParentMenuService.getDelete(id);
            if (tbl_parent_menu == null)
            {
                return HttpNotFound();
            }
            return View(tbl_parent_menu);
        }

        // POST: ParentMenu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            this.ParentMenuService.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
