﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
namespace BusinessEntityManager.Interfaces
{
  public  interface IParentMenuService
    {
        tbl_parent_menu  Details(decimal id);
        tbl_parent_menu Create(tbl_parent_menu tbl_parent_menu);
        tbl_parent_menu getEdit(decimal id);
        tbl_parent_menu postEdit(tbl_parent_menu tbl_parent_menu);
        tbl_parent_menu getDelete(decimal id);
        tbl_parent_menu Delete(decimal id);
    }
}
