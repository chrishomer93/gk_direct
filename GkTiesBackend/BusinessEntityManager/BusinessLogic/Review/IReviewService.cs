﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.BusinessLogic.Review
{
    public interface IReviewService
    {
        bool AddReview(tbl_gk_reviews review);
        List<tbl_gk_reviews> GetReviews();
    }
}
