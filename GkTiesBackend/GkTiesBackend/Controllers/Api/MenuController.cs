﻿using Database;
using GkTiesBackend.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MenuController : ApiController
    {
        private Gk_TiesEntities1 db = new Gk_TiesEntities1();
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetAllMenu()
        {
            return Ok(db.tbl_parent_menu.ToList());
        }
    }
}
