﻿using BusinessEntityManager.Interfaces;
using Database;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Classes
{
    public class SliderService : ISliderService
    {
        public bool AddSlider(tbl_Slider slider)
        {
            return new SliderRepository().Add(slider);
        }

        public bool DeleteSlider(decimal sliderId)
        {
            return new SliderRepository().DeleteRecord(sliderId);
        }

        public tbl_Slider FindSlider(decimal sliderId)
        {
            return new SliderRepository().FirstOrDefault(x => x.Id == sliderId);
        }

        public List<tbl_Slider> GetAllSlider()
        {
            return new SliderRepository().GetAll().ToList();
        }

        public List<tbl_Slider> GetSlider()
        {
            return new SliderRepository().GetAll(x => x.Status == true).OrderBy(x => x.Priroty).ToList();
        }

        public bool UpdateSlider(tbl_Slider slider)
        {
            return new SliderRepository().Update(slider);
        }
    }
}
