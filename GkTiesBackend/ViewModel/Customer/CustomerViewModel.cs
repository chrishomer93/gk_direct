﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GkTiesBackend.ViewModels
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string Images { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }


    }
}