﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Database;

namespace GkTiesBackend.DAL.Repository
{
    public class CustomerRepository:GenericRepository<tbl_gk_User>
    {
        public decimal UserId()
        {
            var Id = string.Empty;
            if (HttpContext.Current.User.Identity is ClaimsIdentity identity)
            {
                Id = identity.FindFirst(ClaimTypes.Name).Value;
                return Convert.ToDecimal(Id);
            }
            else
            {
                return 0;
            }
        }
    }
}