﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntityManager.Interfaces;
using Database;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.ViewModels;

namespace BusinessEntityManager.Classes
{
    public class ProductService : IProductService
    {
        public tbl_gk_Product DeleteProduct(decimal Id)
        {
            var result = new ProductRepositroy().FirstOrDefault(x => x.Id == Id);
            return result;
        }

        public List<tbl_gk_Category> LoadCategory()
        {
            var categoryList = new CategoryRepository().GetAll().ToList();
            return categoryList;
        }

        public tbl_gk_Product LoadProductById(decimal id)
        {
            return new ProductRepositroy().FirstOrDefault(x => x.Id == id);
        }

        public bool SaveNewProduct(tbl_gk_Product product)
        {
            return new ProductRepositroy().Add(product);
        }

        public bool UpdateProduct(tbl_gk_Product product)
        {
            return new ProductRepositroy().Update(product);
        }
    }
}
