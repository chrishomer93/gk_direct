﻿using BusinessEntityManager.Interfaces;
using Database;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Classes
{
    public class CouponService : ICouponService
    {
        public bool AddCoupon(tbl_Coupons coupon)
        {
            return new CouponRepository().Add(coupon);
        }

        public tbl_Coupons FindCoupon(string couponId)
        {
            return new CouponRepository().FirstOrDefault(x => x.fld_CouponCode == couponId);
        }
    }
}
