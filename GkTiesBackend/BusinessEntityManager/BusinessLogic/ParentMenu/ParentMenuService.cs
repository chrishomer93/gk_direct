﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using BusinessEntityManager.Interfaces;
using System.Data.Entity;

namespace BusinessEntityManager.Classes
{
    public class ParentMenuService : IParentMenuService
    {
        public tbl_parent_menu Create(tbl_parent_menu tbl_parent_menu)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.tbl_parent_menu.Add(tbl_parent_menu);
                db.SaveChanges();
                return tbl_parent_menu;
            }
        }

        public tbl_parent_menu Delete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_parent_menu tbl_parent_menu = db.tbl_parent_menu.Find(id);
                db.tbl_parent_menu.Remove(tbl_parent_menu);
                db.SaveChanges();
                return tbl_parent_menu;
            }
        }

        public tbl_parent_menu Details(decimal id)
        {
            using(Gk_TiesEntities1 db =new Gk_TiesEntities1())
            {
                tbl_parent_menu tbl_parent_menu = db.tbl_parent_menu.Find(id);
                return tbl_parent_menu;
            }
        }

        public tbl_parent_menu getDelete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_parent_menu tbl_parent_menu = db.tbl_parent_menu.Find(id);
                return tbl_parent_menu;
            }
        }

        public tbl_parent_menu getEdit(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_parent_menu tbl_parent_menu = db.tbl_parent_menu.Find(id);
                return tbl_parent_menu;
            }
        }

        public tbl_parent_menu postEdit(tbl_parent_menu tbl_parent_menu)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.Entry(tbl_parent_menu).State = EntityState.Modified;
                db.SaveChanges();
                return tbl_parent_menu;
            }
        }
    }
}
