﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntityManager.Interfaces;
using Database;
using GkTiesBackend.ViewModels;
using GkTiesBackend.DAL.Repository;
using CryptHelper;

namespace BusinessEntityManager.Classes
{
    public class CustomerService : ICustomerService
    {
        public List<tbl_gk_Category> LoadCategory()
        {
          return new CategoryRepository().GetAll().ToList();
        }

        public List<tbl_gk_User> loadCustomer()
        {
            return new CustomerRepository().GetAll().ToList();
        }

        public bool SaveCustomer(CustomerViewModel cvm)
        {
            if (cvm.Id == 0)
            {
                var user = new tbl_gk_User();
                user.fld_CreateAt = DateTime.Now;
                user.fld_UpdateAt = DateTime.Now;
                user.fld_Fname = cvm.Fname;
                user.fld_Lname = cvm.Lname;
                user.fld_Email = cvm.Email;
                user.fld_Phone = cvm.Phone;
                user.fld_Password = PasswordHelper.EncodePasswordToBase64(cvm.Password);
                user.fld_Pic = cvm.Images;
                if (cvm.Status == "true")
                {
                    user.fld_Status = true;
                }
                else
                {
                    user.fld_Status = false;

                }

                var AddProduct = new CustomerRepository().Add(user);
                if (AddProduct)
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }

            else if (cvm.Id > 0)
            {
                var fndCustomer = new CustomerRepository().FirstOrDefault(x => x.Id == cvm.Id);
                if (fndCustomer != null)
                {
                    fndCustomer.fld_Fname = cvm.Fname;
                    fndCustomer.fld_UpdateAt = DateTime.Now;
                    fndCustomer.fld_Lname = cvm.Lname;
                    fndCustomer.fld_Email = cvm.Email;
                    fndCustomer.fld_Phone = cvm.Phone;
                    fndCustomer.fld_Password = cvm.Password;
                    fndCustomer.fld_Pic = cvm.Images;
                    if (cvm.Status == "true")
                    {
                        fndCustomer.fld_Status = true;
                    }
                    else
                    {
                        fndCustomer.fld_Status = false;

                    }
                    var updateProduct = new CustomerRepository().Update(fndCustomer);
                    if (updateProduct)
                    {
                        return true;
                    }
                    else
                    {
                        return false;

                    }
                }
                else
                {
                    return false;
                }
            }

            else
            {
                return false;
            }
        }



        public bool DeleteCustomer(decimal Id)
        {
            var fndCustomer = new CustomerRepository().FirstOrDefault(x => x.Id == Id);
            if (fndCustomer != null)
            {
                var Delte = new CustomerRepository().DeleteRecord(fndCustomer.Id);
                if (Delte)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


    }
}

