﻿using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class ProductController : Controller
    {
        public ProductController()
        {

        }
        public ProductController(IProductService _productService)
        {
            this.ProductService = _productService;
        }
        private IProductService ProductService;
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProudctCreate()
        {
            return View();
        }
        public PartialViewResult _LoadProduct()
        {
            var AllProduct = new ProductRepositroy().GetAll().OrderByDescending(x => x.fld_Quentity).ToList();
            return PartialView(AllProduct);
        }

        [HttpPost]
        public bool SaveNewProduct(ProductViewModel cvm)
        {
            if (cvm.Id == 0)
            {
                var product = new tbl_gk_Product();
                product.fld_CreateAt = DateTime.Now;
                product.fld_UpdateAt = DateTime.Now;
                product.fld_Name = cvm.Name;
                product.fld_Description = cvm.Description;
                product.fld_Price = cvm.Price;
                product.fld_Width = cvm.WIdth;
                product.fld_Color = cvm.Color;
                product.fld_Style = cvm.Style;
                product.fld_Pattern = cvm.Pattern;
                product.fld_ItemCode = cvm.ItemCode;
                product.fld_Images = cvm.Images;
                product.fld_Quentity = cvm.Quentity;
                product.fld_Option = cvm.Option;
                product.fld_CategoryId = cvm.CategoryId;
                product.fld_Material = cvm.Material;
                var AddProduct = this.ProductService.SaveNewProduct(product);
                if (AddProduct)
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }
            else if (cvm.Id > 0)
            {
                var findProudct = this.ProductService.LoadProductById(cvm.Id);
                if (findProudct != null)
                {
                    findProudct.fld_Name = cvm.Name;
                    findProudct.fld_UpdateAt = DateTime.Now;
                    findProudct.fld_Name = cvm.Name;
                    findProudct.fld_Description = cvm.Description;
                    findProudct.fld_Price = cvm.Price;
                    findProudct.fld_Width = cvm.WIdth;
                    findProudct.fld_Color = cvm.Color;
                    findProudct.fld_Style = cvm.Style;
                    findProudct.fld_Pattern = cvm.Pattern;
                    findProudct.fld_Images = cvm.Images;
                    findProudct.fld_Quentity = cvm.Quentity;
                    findProudct.fld_Option = cvm.Option;
                    findProudct.fld_CategoryId = cvm.CategoryId;
                    findProudct.fld_ItemCode = cvm.ItemCode;
                    findProudct.fld_Material = cvm.Material;
                    var updateProduct = this.ProductService.UpdateProduct(findProudct);
                    if (updateProduct)
                    {
                        return true;
                    }
                    else
                    {
                        return false;

                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        [HttpPost]
        public bool DeleteProduct(decimal Id)
        {
            var findProudct = this.ProductService.DeleteProduct(Id);
            if (findProudct != null)
            {
                var Delte = new ProductRepositroy().DeleteRecord(findProudct.Id);
                if (Delte)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public ActionResult Upload()
        {
            string path = string.Empty;
            List<string> filePath = new List<string>();
            if (Request.Files != null)
                try
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];

                        var fileName = Path.GetFileName(file.FileName);
                        var name = fileName.Split('.')[0];
                        var extension = fileName.Split('.')[1];
                        Random rand = new Random();
                        var r = rand.Next(1, 99999);


                        name = name + r;
                        fileName = name + "." + extension;
                        path = Path.Combine(Server.MapPath("/Images/Product"), fileName);
                        filePath.Add(fileName);
                        file.SaveAs(path);

                    }
                }
                catch (Exception)
                {

                }
            else
            {
                return Json("false");
            }
            return Json(filePath);

        }

        [HttpPost]
        public JsonResult LoadCategory()
        {
            var res = "";
            var drplist = "";
            try
            {

                var categoryList = this.ProductService.LoadCategory();
                if (categoryList.Any())
                {
                    drplist += "<option selected disabled value=''>Select Category</option>";
                    foreach (var item in categoryList)
                    {
                        if (item != null)
                        {
                            drplist += "<option value='" + item.Id + "'>" + item.fld_Name + "</option>";
                        }
                    }
                    res = "true";
                }
                else
                {
                    drplist += "<option value=''></option>";
                    res = "false";
                }

            }
            catch (Exception)
            {
                res = "false";
            }
            return Json(new { res = res, drpmarlasetting = drplist });
        }
    }
}