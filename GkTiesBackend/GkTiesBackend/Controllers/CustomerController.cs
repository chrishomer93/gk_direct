﻿using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class CustomerController : Controller
    {

        public CustomerController()
        {

        }
        public CustomerController(ICustomerService _customerService)
        {
            this.customerService = _customerService;
        }

        private ICustomerService customerService;

        // GET: Category
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProudctCreate()
        {
            return View();
        }
        public PartialViewResult _LoadCustomer()
        {
            var allProducts = this.customerService.loadCustomer();
            return PartialView(allProducts);
        }

        [HttpPost]
        public bool SaveCustomer(CustomerViewModel cvm)
        {
            var save = this.customerService.SaveCustomer(cvm);
            return save;
        }

        [HttpPost]
        public bool DeleteCustomer(decimal Id)
        {
            var delete = this.customerService.DeleteCustomer(Id);
            return delete;
        }
        public ActionResult Upload()
        {
            string path = string.Empty;
            List<string> filePath = new List<string>();
            if (Request.Files != null)
                try
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];

                        var fileName = Path.GetFileName(file.FileName);
                        var name = fileName.Split('.')[0];
                        var extension = fileName.Split('.')[1];
                        Random rand = new Random();
                        var r = rand.Next(1, 99999);


                        name = name + r;
                        fileName = name + "." + extension;
                        path = Path.Combine(Server.MapPath("/Images/Product"), fileName);
                        filePath.Add(fileName);
                        file.SaveAs(path);
                    }
                }
                catch (Exception)
                {

                }
            else
            {
                return Json("false");
            }
            return Json(filePath);

        }

        [HttpPost]
        public JsonResult LoadCategory()
        {
            var res = "";
            var drplist = "";
            try
            {
                var categoryList = this.customerService.LoadCategory();
                if (categoryList.Any())
                {
                    drplist += "<option selected disabled value=''>Select Category</option>";
                    foreach (var item in categoryList)
                    {
                        if (item != null)
                        {
                            drplist += "<option value='" + item.Id + "'>" + item.fld_Name + "</option>";
                        }
                    }
                    res = "true";
                }
                else
                {
                    drplist += "<option value=''></option>";
                    res = "false";
                }

            }
            catch (Exception)
            {
                res = "false";
            }
            return Json(new { res = res, drpmarlasetting = drplist });
        }
    }
}