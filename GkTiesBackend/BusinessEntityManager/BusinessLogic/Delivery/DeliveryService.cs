﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntityManager.Interfaces;
using Database;

namespace BusinessEntityManager.Classes
{
    public class DeliveryService : IDeliveryService
    {
        public tbl_Delivery_Package create(tbl_Delivery_Package pkg)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
               db.tbl_Delivery_Package.Add(pkg);
                db.SaveChanges();
                return pkg;
            }
        }

        public tbl_Delivery_Package delete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_Delivery_Package tbl_Delivery_Package = db.tbl_Delivery_Package.Find(id);
                db.tbl_Delivery_Package.Remove(tbl_Delivery_Package);
                db.SaveChanges();
                return tbl_Delivery_Package;
            }
        }

        public tbl_Delivery_Package detials(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_Delivery_Package pckg = db.tbl_Delivery_Package.Find(id);
                return pckg;
            }
        }

        public tbl_Delivery_Package edit(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_Delivery_Package tbl_Delivery_Package = db.tbl_Delivery_Package.Find(id);
                return tbl_Delivery_Package;
            }
        }

        public tbl_Delivery_Package findDelete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_Delivery_Package tbl_Delivery_Package = db.tbl_Delivery_Package.Find(id);
                return tbl_Delivery_Package;
            }
        }

        public tbl_Delivery_Package postEdit(tbl_Delivery_Package tbl_Delivery_Package)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                db.Entry(tbl_Delivery_Package).State = EntityState.Modified;
                db.SaveChanges();
                return tbl_Delivery_Package;
            }
        }
    }
}
