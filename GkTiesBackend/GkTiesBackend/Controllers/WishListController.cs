﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GkTiesBackend.DAL;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class WishListController : Controller
    {
        private readonly IWishListService wishListService;
        public WishListController()
        {

        }
        public WishListController(IWishListService _wishListService)
        {
            this.wishListService = _wishListService;
        }

        private Gk_TiesEntities1 db = new Gk_TiesEntities1();

        // GET: WishList
        public ActionResult Index()
        {
            var tbl_gk_WishList = this.wishListService.GetAllWishList();
            return View(tbl_gk_WishList);
        }

        // GET: WishList/Details/5
        public ActionResult Details(decimal id)
        {
            tbl_gk_WishList tbl_gk_WishList = this.wishListService.GetWishListbyId(id);
            if (tbl_gk_WishList == null)
            {
                return HttpNotFound();
            }
            return View(tbl_gk_WishList);
        }

        // GET: WishList/Create
        public ActionResult Create()
        {
            ViewBag.fld_Product_Id = new SelectList(db.tbl_gk_Product, "Id", "fld_Name");
            ViewBag.fld_User_Id = new SelectList(db.tbl_gk_User, "Id", "fld_Fname");
            return View();
        }

        // POST: WishList/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,fld_User_Id,fld_Product_Id,fld_CreateAt,fld_Quntity")] tbl_gk_WishList tbl_gk_WishList)
        {
            if (ModelState.IsValid)
            {
                this.wishListService.AddWishList(tbl_gk_WishList);
                return RedirectToAction("Index");
            }

            ViewBag.fld_Product_Id = new SelectList(db.tbl_gk_Product, "Id", "fld_Name", tbl_gk_WishList.fld_Product_Id);
            ViewBag.fld_User_Id = new SelectList(db.tbl_gk_User, "Id", "fld_Fname", tbl_gk_WishList.fld_User_Id);
            return View(tbl_gk_WishList);
        }

        // GET: WishList/Edit/5
        public ActionResult Edit(decimal id)
        {
            tbl_gk_WishList tbl_gk_WishList = this.wishListService.GetWishListbyId(id);
            if (tbl_gk_WishList == null)
            {
                return HttpNotFound();
            }
            ViewBag.fld_Product_Id = new SelectList(db.tbl_gk_Product, "Id", "fld_Name", tbl_gk_WishList.fld_Product_Id);
            ViewBag.fld_User_Id = new SelectList(db.tbl_gk_User, "Id", "fld_Fname", tbl_gk_WishList.fld_User_Id);
            return View(tbl_gk_WishList);
        }

        // POST: WishList/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,fld_User_Id,fld_Product_Id,fld_CreateAt,fld_Quntity")] tbl_gk_WishList tbl_gk_WishList)
        {
            if (ModelState.IsValid)
            {
                this.wishListService.UpdateWishList(tbl_gk_WishList);
                return RedirectToAction("Index");
            }
            ViewBag.fld_Product_Id = new SelectList(db.tbl_gk_Product, "Id", "fld_Name", tbl_gk_WishList.fld_Product_Id);
            ViewBag.fld_User_Id = new SelectList(db.tbl_gk_User, "Id", "fld_Fname", tbl_gk_WishList.fld_User_Id);
            return View(tbl_gk_WishList);
        }

        // GET: WishList/Delete/5
        public ActionResult Delete(decimal id)
        {
            tbl_gk_WishList tbl_gk_WishList = this.wishListService.GetWishListbyId(id);
            if (tbl_gk_WishList == null)
            {
                return HttpNotFound();
            }
            return View(tbl_gk_WishList);
        }

        // POST: WishList/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            tbl_gk_WishList tbl_gk_WishList = this.wishListService.GetWishListbyId(id);
            this.wishListService.DeleteWishList(tbl_gk_WishList);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
