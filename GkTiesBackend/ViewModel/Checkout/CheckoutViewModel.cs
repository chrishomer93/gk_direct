﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GkTiesBackend.Models
{
    public class CheckoutViewModel
    {
        public string fld_Country { get; set; }
        public string fld_City { get; set; }
        public string fld_State { get; set; }
        public string fld_Zip { get; set; }
        public string fld_Fname { get; set; }
        public string fld_Lname { get; set; }
        public string fld_Company { get; set; }
        public string fld_Address1 { get; set; }
        public string fld_Address2 { get; set; }
        public string fld_Email { get; set; }
        public string fld_Phone { get; set; }
        public string fld_Proudt_Id { get; set; }
        public string fld_UnitPrice { get; set; }
        public string fld_Quntity { get; set; }
        public string fld_Discount { get; set; }
        public string fld_OrderNote { get; set; }
        public string fld_Total { get; set; }
        public string token { get; set; }
        public string transactionId { get; set; }
    }
}