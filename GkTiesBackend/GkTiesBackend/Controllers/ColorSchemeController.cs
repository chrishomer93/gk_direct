﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Database;
using GkTiesBackend.DAL;
using BusinessEntityManager.Interfaces;
namespace GkTiesBackend.Controllers
{
    public class ColorSchemeController : Controller
    {

        public ColorSchemeController()
        {

        }

        public ColorSchemeController(IColorSchemeService _colorSchemeService)
        {
            this.colorSchemeService = _colorSchemeService;
        }

        private IColorSchemeService colorSchemeService;
        private Gk_TiesEntities1 db = new Gk_TiesEntities1();

        // GET: ColorScheme
        public ActionResult Index()
        {
            return View(db.tbl_ColorScheme.ToList());
        }

        // GET: ColorScheme/Details/5
        public ActionResult Details(decimal id)
        {
                var colorScheme = this.colorSchemeService.details(id);

                if (colorScheme == null)
                {
                    return HttpNotFound();
                }
                return View(colorScheme);
            }
    

        // GET: ColorScheme/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Color,Active")] tbl_ColorScheme tbl_ColorScheme)
        {
            if (ModelState.IsValid)
            {
                var createColorScheme = this.colorSchemeService.createColorScheme(tbl_ColorScheme);
                if (createColorScheme!= null)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(tbl_ColorScheme);
        }

        // GET: ColorScheme/Edit/5
        public ActionResult Edit(decimal id)
        {
                var editColorScheme = this.colorSchemeService.editColorScheme(id);
                if (editColorScheme == null)
                {
                    return HttpNotFound();
                }
                return View(editColorScheme);
            }
        

        public ActionResult ChangeTheme (decimal id)
        {
            var theme = this.colorSchemeService.changeColorTheme(id);

            if (theme == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Index");
        }

        // POST: ColorScheme/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Color,Active")] tbl_ColorScheme tbl_ColorScheme)
        {
            if (ModelState.IsValid)
            { 
                db.Entry(tbl_ColorScheme).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_ColorScheme);
        }

        // GET: ColorScheme/Delete/5
        public ActionResult Delete(decimal id)
        {
            var findDeleteId = this.colorSchemeService.findDeleteId(id);         
            if (findDeleteId == null)
            {
                return HttpNotFound();
            }
            return View(findDeleteId);
        }

        // POST: ColorScheme/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            var delete = this.colorSchemeService.delete(id);
            if (delete !=null )
            {
                return RedirectToAction("Index");
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        }

    }

