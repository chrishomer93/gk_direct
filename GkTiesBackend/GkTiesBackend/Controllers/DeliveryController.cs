﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GkTiesBackend.DAL;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class DeliveryController : Controller
    {
        public DeliveryController()
        {

        }
        public DeliveryController(IDeliveryService _deliveryService)
        {
            this.deliveryService = _deliveryService;
        }

        private IDeliveryService deliveryService;
        private Gk_TiesEntities1 db = new Gk_TiesEntities1();

        // GET: Delivery
        public ActionResult Index()
        {
            return View(db.tbl_Delivery_Package.ToList());
        }

        // GET: Delivery/Details/5
        public ActionResult Details(decimal id)
        {

            tbl_Delivery_Package tbl_Delivery_Package = this.deliveryService.detials(id);
            if (tbl_Delivery_Package == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Delivery_Package);
        }

        // GET: Delivery/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Delivery/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,fld_title,fld_Amount,fld_Description")] tbl_Delivery_Package tbl_Delivery_Package)
        {
            if (ModelState.IsValid)
            {
                var create = this.deliveryService.create(tbl_Delivery_Package); 
                return RedirectToAction("Index");
            }

            return View(tbl_Delivery_Package);
        }

        // GET: Delivery/Edit/5
        public ActionResult Edit(decimal id)
        {
            tbl_Delivery_Package tbl_Delivery_Package = this.deliveryService.edit(id);
            if (tbl_Delivery_Package == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Delivery_Package);
        }

        // POST: Delivery/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,fld_title,fld_Amount,fld_Description")] tbl_Delivery_Package tbl_Delivery_Package)
        {
            if (ModelState.IsValid)
            {
                this.deliveryService.postEdit(tbl_Delivery_Package);
                return RedirectToAction("Index");
            }
            return View(tbl_Delivery_Package);
        }

        // GET: Delivery/Delete/5
        public ActionResult Delete(decimal id)
        {
            var tbl_Delivery_Package = this.deliveryService.findDelete(id);
            if (tbl_Delivery_Package == null)
            {
                return HttpNotFound();
            }
            return View(tbl_Delivery_Package);
        }

        // POST: Delivery/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            var delete = this.deliveryService.delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
