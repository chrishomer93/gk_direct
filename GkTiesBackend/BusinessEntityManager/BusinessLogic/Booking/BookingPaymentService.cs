﻿using BusinessEntityManager.Interfaces;
using Database;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Classes
{
    public class BookingPaymentService : IBookingPaymentService
    {
        public bool AddCheckOutDetails(tbl_gk_Checkout checkout)
        {
            return new CheckoutRepository().Add(checkout);
        }


        public bool AddShippingDetails(tbl_gk_ShippingDetails shippingDetails)
        {
            return new ShippingDetailsRepository().Add(shippingDetails);
        }

        public decimal CustomerUserId()
        {
            return new CustomerRepository().UserId();
        }

        public List<tbl_gk_Checkout> GetOrderHistory(string email)
        {
            var userId = this.CustomerUserId();
            return new CheckoutRepository().GetAllQueryable(x => x.fld_User_Id == userId).ToList();
        }
    }
}
