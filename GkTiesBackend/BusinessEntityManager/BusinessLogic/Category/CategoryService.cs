﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using BusinessEntityManager.Interfaces;
using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.ViewModels;
using System.Web;
using System.IO;

namespace BusinessEntityManager.Classes
{
    public class categoryService : ICategoryService
    {
        public bool saveCategory(CategoryViewModal cvm)
        {
            if (cvm.Id == 0)
            {
                var category = new tbl_gk_Category();
                category.fld_CreateAt = DateTime.Now;
                category.fld_UpdateAt = DateTime.Now;
                category.fld_Name = cvm.Name;
                category.fld_Description = cvm.Description;
                category.fld_Image = cvm.Images;
                var AddCategory = new CategoryRepository().Add(category);
                if (AddCategory)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            else if (cvm.Id > 0)
            {
                var findCategory = new CategoryRepository().FirstOrDefault(x => x.Id == cvm.Id);
                if (findCategory != null)
                {
                    findCategory.fld_Name = cvm.Name;
                    findCategory.fld_Description = cvm.Description;
                    findCategory.fld_Image = cvm.Images;
                    findCategory.fld_UpdateAt = DateTime.Now;
                    var updateCategory = new CategoryRepository().Update(findCategory);
                    if (updateCategory)
                    {
                        return true;
                    }
                    else
                    {
                        return false;

                    }
                }
                else
                {
                    return false;
                }
            }

            else
            {
                return false;
            }

        }

        public bool deleteCategory(decimal Id)
        {
            var FindCategory = new CategoryRepository().FirstOrDefault(x => x.Id == Id);
            if (FindCategory != null)
            {
                var Delte = new CategoryRepository().DeleteRecord(FindCategory.Id);
                if (Delte)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool uploadCategory()
        {
            return true;
        }
    }
}