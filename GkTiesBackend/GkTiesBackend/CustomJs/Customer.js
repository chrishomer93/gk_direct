﻿$(document).ready(function () {
    loadProduct();
    LoadCategoryList();
});
var CreateProudct = function () {
    $("#Fname").val("");
    $("#Lname").val("");
    $("#Email").val("");
    $("#Phone").val("");
    $("#Password").val("");
    $("#Tags").val("");
    $("#thumbnil").attr("src", "");
  
    $("#btnForm").prop('value', 'Create');
    $("#large").modal();
    $("#Id").val(0);
};
var loadProduct = function () {
    var ur = $("#hidurlOfCategory").val();
    $.ajax({
        url: ur,
        type: "Get",
        success: function (res) {
            $("#loadProductTable").html(res);
            $("#tblProduct").DataTable();
        }
    });

};
var RegisterNewProduct = function () {
    var image = saveImage();
    if (typeof image !== "undefined") {
        $("#Images").val(image);
    }
    var category = $("#drpcategory").val();
    if (category !== "") {
        $("#CategoryId").val(category);
        var isFormValid = $(".form-horizontal").valid();
        if (isFormValid) {
            $.ajax({
                url: $("#myfrom").attr('action'),
                type: $("#myfrom").attr('method'),
                data: $("#myfrom").serialize(),
                success: function (res) {

                    if (res == "True") {
                        $.toast({
                            heading: 'Customer Update',
                            text: 'Customer save to database successfully',
                            icon: 'info',
                            loader: true,        // Change it to false to disable loader
                            loaderBg: '#9EC600',  // To change the background
                            position: 'bottom-right'
                        });
                        loadProduct();

                        $("#large").modal("hide");
                    }
                    else {
                        $.toast({
                            heading: 'Customer Not Update',
                            text: 'Customer  Not save to database successfully',
                            icon: 'error',
                            loader: true,        // Change it to false to disable loader
                            loaderBg: '#9EC600',  // To change the background
                            position: 'bottom-right'
                        });
                    }
                }

            });
        }
    } else {
        $.toast({
            heading: 'Category Not Select',
            text: 'Category  Not Select',
            icon: 'error',
            loader: true,        // Change it to false to disable loader
            loaderBg: '#9EC600',  // To change the background
            position: 'bottom-right'
        });
    }

};
var UpdateCustomer = function (id, fname,lname,email,phone,password,status, image) {
    $("#large").modal();
    $("#Id").val(id);
    $("#Fname").val(fname);
    $("#Lname").val(lname);
    $("#Email").val(email);
    $("#Phone").val(phone);
    $("#Password").val(password);
    $("#Status").val(status);
    $("#Images").val(image);
    $("#thumbnil").attr("src", "Images/Product/" + image);
    $("#btnForm").prop('value', 'Update');
};
var DeleteCustomer = function (id) {
    var Geturl = $("#hidurldeleteCustomer").val();

    $.ajax({
        url: Geturl,
        type: "Post",
        data: { id: id },
        success: function (res) {
            if (res == "True") {
                $.toast({
                    heading: 'Customer Delete',
                    text: 'Customer Delete successfully',
                    icon: 'info',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
                loadProduct();
            }
            else {
                $.toast({
                    heading: 'Customer Not Delete',
                    text: 'Category  Not Delete',
                    icon: 'error',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    position: 'bottom-right'
                });
            }
        }

    })


}
var saveImage = function () {
    var url = $("#hidurlImageUpload").val();
    var imageName;
    var formData = new FormData();
    var totalFiles = document.getElementById("ImgUpload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("ImgUpload").files[i];

        formData.append("ImgUpload", file);
    }
    if (totalFiles > 0) {
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            data: formData,
            success: function (res) {
                imageName = res;
            }
        });
    }
    return imageName;
};
function showMyImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {
            continue;
        }
        var img = document.getElementById("thumbnil");

        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
}
var LoadCategoryList = function () {
    var message = "";
    var getUrl = $("#hidurlLoadCategory").val();
    $.ajax({
        url: getUrl,
        type: "POST",
        success: function (msg) {
            if (msg.res === "true") {
                $("#drpcategory").html(msg.drpmarlasetting);

            }


        },
        error: function (res) {

        }
    });
};
  