﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmailHelper
{
    public class EmailSender
    {
        public static bool SendEmail(string senderEmail, string senderPass, string subject, string toEmail, string body)
        {
            string MailHostServer = "gkdirect.co.uk";
            string port = "25";
            string displayName = subject;

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(senderEmail, displayName);
            mail.To.Add(toEmail);
            mail.Subject = subject;
            mail.IsBodyHtml = true;
            mail.Body = body;
            mail.Priority = MailPriority.High;
            var stmp = new SmtpClient();
            stmp.Credentials = new NetworkCredential(senderEmail, senderPass);
            stmp.Host = MailHostServer;
            stmp.Port = Convert.ToInt32(port);
            stmp.EnableSsl = false;
            stmp.Send(mail);
            return true;
        }
    }
}
