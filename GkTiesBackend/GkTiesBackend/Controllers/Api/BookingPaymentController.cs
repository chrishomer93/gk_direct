﻿using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.Helper;
using GkTiesBackend.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Database;
using BusinessEntityManager.Interfaces;
using EmailHelper;
using System.Configuration;
using BusinessEntityManager.Classes;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BookingPaymentController : ApiController
    {
        private readonly IBookingPaymentService bookingPaymentService;
        public BookingPaymentController(IBookingPaymentService _bookingPaymentService)
        {
            this.bookingPaymentService = _bookingPaymentService;
        }

        public string MakePayment(CheckoutViewModel shippingDetails)
        {
            try
            {
                //StripeConfiguration.ApiKey = "sk_live_...tgkI"; //Live API Key
                StripeConfiguration.ApiKey = "sk_test_eH5Ni6YrX8uwvGxhPk08TzuV00QyoLO7AA";
                var customers = new Stripe.CustomerService();
                var charges = new ChargeService();

                var customer = customers.Create(new CustomerCreateOptions
                {
                    Email = shippingDetails.fld_Email,
                    Source = shippingDetails.token
                });
                var charge = charges.Create(new ChargeCreateOptions
                {
                    Amount = Convert.ToInt64(shippingDetails.fld_Total),
                    Description = "some bunch of ties purchased, from gkdirect.co",
                    Currency = "GBP",
                    Customer = customer.Id
                });
                if (charge.Status == "succeeded")
                {
                    return charge.BalanceTransactionId.ToString();
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public IHttpActionResult FetchOrderHistory(string userEmail)
        {
            var orderHistory = this.bookingPaymentService.GetOrderHistory(userEmail);
            return Ok(orderHistory);
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult SaveShippingDetails(CheckoutViewModel shippingDetails)
        {
            try
            {
                var authService = new AuthorizationService();

                var sd = new tbl_gk_ShippingDetails();
                sd.fld_Fname = shippingDetails.fld_Fname;
                sd.fld_Lname = shippingDetails.fld_Lname;
                sd.fld_Email = shippingDetails.fld_Email;
                sd.fld_Phone = shippingDetails.fld_Phone;
                sd.fld_State = shippingDetails.fld_State;
                sd.fld_Address1 = Convert.ToString(shippingDetails.fld_Address1);
                sd.fld_Address2 = Convert.ToString(shippingDetails.fld_Address2);
                sd.fld_City = shippingDetails.fld_City;
                sd.fld_Company = shippingDetails.fld_Company;
                sd.fld_Country = Convert.ToString(shippingDetails.fld_Country);
                sd.fld_CreateAt = DateTime.Now;
                sd.fld_Zip = shippingDetails.fld_Zip;
                var user = authService.FindUser(sd.fld_Email);
                sd.fld_User_Id = user.Id;
                var add = this.bookingPaymentService.AddShippingDetails(sd);
                var response = MakePayment(shippingDetails);

                if (response != "false")
                {
                    var senderEmail = ConfigurationManager.AppSettings["SenderEmail"];
                    var senderPassword = ConfigurationManager.AppSettings["SenderPassword"];
                    var emailContent = "<html><head><style>body{font-family:Tahoma;font-size:11px}</style></head><body><h3> Dear [customerName]</h3><p> Thank you for shopping with GK, please find below details of your order.</p><p> Your order reference number is: [orderNumber]</p><table border='1'><tr><td><b>Customer Name:</b></td><td>[customerName]</td></tr><tr><td><b>Delivery Address:</b></td><td>[shippingAddress]</td></tr><tr><td><b>Total Amount:</b></td><td>[totalAmount]</td></tr></table><p> Once your order has been processed and dispatched, we’ll send you another email. If you have any queries regarding your order, please contact customer service at <a href='#'>customerservice@gkdirect.co.uk</a> and we’ll be happy to help.</p><p> Thank you for shopping with us at GK.</p></body></html>";
                    emailContent=emailContent.Replace("[customerName]", shippingDetails.fld_Fname + " " + shippingDetails.fld_Lname)
                        .Replace("[orderNumber]", response)
                        .Replace("[shippingAddress]", shippingDetails.fld_Address2)
                        .Replace("[totalAmount]", shippingDetails.fld_Total);

                    EmailSender.SendEmail(senderEmail, senderPassword, "Your order reference number is " + response, sd.fld_Email, emailContent);
                    var ProductArray = shippingDetails.fld_Proudt_Id.Split('-');
                    var QunaityArrary = shippingDetails.fld_Quntity.Split('-');
                    var UnitPriceArray = shippingDetails.fld_UnitPrice.Split('-');

                    for (int i = 0; i < (ProductArray.Length - 1); i++)
                    {
                        var checkout = new tbl_gk_Checkout();
                        checkout.fld_Token = shippingDetails.token;
                        checkout.fld_Transaction_Id = response;
                        checkout.fld_CreateAt = DateTime.Now;
                        checkout.fld_Discount = Convert.ToDecimal(shippingDetails.fld_Discount);
                        checkout.fld_OrderNote = shippingDetails.fld_OrderNote;
                        checkout.fld_Proudt_Id = Convert.ToDecimal(ProductArray[i]);
                        checkout.fld_Quntity = Convert.ToDecimal(QunaityArrary[i]);
                        checkout.fld_Shipping_Details = sd.Id;
                        checkout.fld_Total = Convert.ToDecimal(shippingDetails.fld_Total);
                        checkout.fld_UnitPrice = Convert.ToDecimal(UnitPriceArray[i]);
                        var findUserCheckout = authService.FindUser(sd.fld_Email);
                        checkout.fld_User_Id = user.Id;
                        var addCheckout = this.bookingPaymentService.AddCheckOutDetails(checkout);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Ok();

        }
    }
}
