﻿using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Database;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers
{
    public class SliderController : Controller
    {
        private readonly ISliderService _sliderService;
        public SliderController()
        {

        }

        public SliderController(ISliderService sliderService)
        {
            this._sliderService = sliderService;
        }

        // GET: Category
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult _LoadSlider()
        {
            var AllCategoryList = this._sliderService.GetAllSlider();
            return PartialView(AllCategoryList);
        }
        public ActionResult CreateView()
        {
            return View();
        }
        [HttpPost]
        public bool SaveNewSlider(SliderViewModel cvm)
        {
            if (cvm.Id == 0)
            {
                var slider = new tbl_Slider();
                slider.CreateAt = DateTime.Now;
                slider.UpdateAt = DateTime.Now;
                slider.Title = cvm.Title;
                slider.Priroty = cvm.Preiorty;
                slider.Status = true;
                slider.Image = cvm.Image;
                var AddSlider = this._sliderService.AddSlider(slider);
                if (AddSlider)
                {
                    return true;
                }
                else
                {
                    return false;

                }
            }
            else if (cvm.Id > 0)
            {
                var findSlider = this._sliderService.FindSlider(cvm.Id);
                if (findSlider != null)
                {
                    findSlider.Title = cvm.Title;
                    findSlider.Priroty = cvm.Preiorty;
                    findSlider.Image = cvm.Image;
                    findSlider.UpdateAt = DateTime.Now;
                    var updateSlider = this._sliderService.UpdateSlider(findSlider);
                    if (updateSlider)
                    {
                        return true;
                    }
                    else
                    {
                        return false;

                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
        [HttpPost]
        public bool DeleteSlider(decimal Id)
        {
            var findSlider = this._sliderService.FindSlider(Id);
            if (findSlider != null)
            {
                var Delte = this._sliderService.DeleteSlider(findSlider.Id);
                if (Delte)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        [HttpPost]
        public bool UpdateSlider(decimal Id)
        {
            var findSlider = this._sliderService.FindSlider(Id);
            if (findSlider != null)
            {
                if (findSlider.Status == true)
                {
                    findSlider.Status = false;
                }
                else
                {
                    findSlider.Status = true;
                }
                var Delte = this._sliderService.UpdateSlider(findSlider);
                if (Delte)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public ActionResult Upload()
        {
            string path = string.Empty;
            List<string> filePath = new List<string>();
            if (Request.Files != null)
                try
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];

                        var fileName = Path.GetFileName(file.FileName);
                        var name = fileName.Split('.')[0];
                        var extension = fileName.Split('.')[1];
                        Random rand = new Random();
                        var r = rand.Next(1, 99999);


                        name = name + r;
                        fileName = name + "." + extension;
                        path = Path.Combine(Server.MapPath("/Images/Slider"), fileName);
                        filePath.Add(fileName);
                        file.SaveAs(path);

                    }
                }
                catch (Exception)
                {

                }
            else
            {
                return Json("false");
            }
            return Json(filePath);

        }
    }
}