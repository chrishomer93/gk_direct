﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GkTiesBackend.ViewModels
{
    public class SliderViewModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public int Preiorty { get; set; }
        [Required]
        public string Image { get; set; }
        [Required]
        public bool Status { get; set; }
        public decimal Id { get; set; }
    }
}