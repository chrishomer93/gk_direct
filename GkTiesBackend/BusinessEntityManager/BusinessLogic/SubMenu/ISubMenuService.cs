﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
namespace BusinessEntityManager.Interfaces
{
  public  interface ISubMenuService
    {
        tbl_SubMenu Details(decimal id);
        tbl_SubMenu Create(tbl_SubMenu tbl_SubMenu);
        tbl_SubMenu getEdit(decimal id);
        tbl_SubMenu postEdit(tbl_SubMenu tbl_SubMenu);
        tbl_SubMenu getDelete(decimal id);
        tbl_SubMenu Delete(decimal id);
    }
}
