﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Interfaces
{
    public interface IAuthorizationService
    {
        bool AddUser(tbl_gk_User user);
        tbl_gk_User FindUser(string email, string password = "");
    }
}
