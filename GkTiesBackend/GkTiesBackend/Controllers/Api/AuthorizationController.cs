﻿using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.Helper;
using GkTiesBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Database;
using BusinessEntityManager.Interfaces;
using CryptHelper;
using EmailHelper;
using System.Configuration;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuthorizationController : ApiController
    {
        private readonly IAuthorizationService authorizationService;

        public AuthorizationController()
        {

        }

        public AuthorizationController(IAuthorizationService _authorizationService)
        {
            this.authorizationService = _authorizationService;
        }

        [JwtAuthentication]
        [HttpGet]
        public IEnumerable<string> Test()
        {
            return new string[] { "APi Working", "Fine" };
        }
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult RegisterCustomer(CustomerViewModel cvm)
        {
            var findemail = new CustomerRepository().FirstOrDefault(x => x.fld_Email == cvm.Email);
            if (findemail == null)
            {
                var user = new tbl_gk_User();
                user.fld_CreateAt = DateTime.Now;
                user.fld_UpdateAt = DateTime.Now;
                user.fld_Email = cvm.Email;
                user.fld_Fname = cvm.Fname;
                user.fld_Lname = cvm.Lname;
                user.fld_Password = PasswordHelper.EncodePasswordToBase64(cvm.Password);
                user.fld_Phone = cvm.Phone;
                user.fld_Pic = cvm.Images;
                user.fld_LoginType = cvm.Type;
                user.fld_Status = true;
                var AddUser = this.authorizationService.AddUser(user);
                if (AddUser)
                {
                    var senderEmail = ConfigurationManager.AppSettings["SenderEmail"];
                    var senderPassword = ConfigurationManager.AppSettings["SenderPassword"];
                    EmailSender.SendEmail(senderEmail, senderPassword, "Welcome to GkDirect", user.fld_Email, "<h2>Welcome to GkDirect</h2><p>Thanks for registering into our website.</p>");
                    return Ok(new { status = true, Message = "Successfully create" });
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return NotFound();
            }
        }

        // THis is naive endpoint for demo, it should use Basic authentication to provide token or POST request
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult Login([FromBody] User user)
        {
            if (ModelState.IsValid)
            {
                if (CheckUser(user.email, user.password))
                {
                    var findUser = this.authorizationService.FindUser(user.email, user.password);

                    return Ok(new { success = true, token = "Bearer " + JwtManager.GenerateToken(findUser.Id.ToString()), user = findUser.fld_Fname + " " + findUser.fld_Lname });
                }
                else
                {
                    return Ok(new { success = false, msg = "Login attempt failed! Invalid credentials" });

                }
            }


            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }

        public bool CheckUser(string username, string password)
        {
            var findUser = this.authorizationService.FindUser(username, password);
            if (findUser != null)
            {

                return true;
            }
            return false;
        }
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult CheckEmail([FromBody] User user)
        {
            var findUser = this.authorizationService.FindUser(user.email);
            if (findUser != null)
            {
                return Ok(new { success = false, msg = "Already Exist Email" });

            }
            else
            {
                return Ok(new { success = true, msg = "Email Not Exist" });
            }
        }
    }
}
public class User
{
    [Required]
    public string email { get; set; }
    [Required]
    public string password { get; set; }
}