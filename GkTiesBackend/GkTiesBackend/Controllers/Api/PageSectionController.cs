﻿using GkTiesBackend.DAL;
using GkTiesBackend.DAL.Repository;
using GkTiesBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Cors;
using Database;
using EmailHelper;
using BusinessEntityManager.Interfaces;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PageSectionController : ApiController
    {
        private readonly IPageSectionService pageSectionService;
        public PageSectionController(IPageSectionService _pageSectionService)
        {
            this.pageSectionService = _pageSectionService;
        }

        private Gk_TiesEntities1 db = new Gk_TiesEntities1();
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetThemeColor()
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                var scheme = db.tbl_ColorScheme.Where(x => x.Active == true).FirstOrDefault();
                return Ok(scheme.Color);
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetMailSection()
        {
            return Ok(this.pageSectionService.GetPageSection("MailList"));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetCollectSection()
        {
            return Ok(this.pageSectionService.GetPageSection("Collection"));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetNewArrivalSection()
        {
            return Ok(this.pageSectionService.GetPageSection("NewArrivals"));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetFooterSection()
        {
            return Ok(this.pageSectionService.GetPageSection("Footer"));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetServiceSection()
        {
            return Ok(this.pageSectionService.GetPageSection("Service"));
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult GetDeliveryOption()
        {
            return Ok(db.tbl_Delivery_Package.ToList());
        }
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult MailSubscriber(string mail)
        {
            try
            {
                var findMail = db.tbl_MailSubscriber.FirstOrDefault(x => x.Mail.ToLower() == mail.ToLower());
                if (findMail == null)
                {
                    var mailsubscirb = new tbl_MailSubscriber();
                    mailsubscirb.CreateAt = DateTime.Now;
                    mailsubscirb.Status = false;
                    mailsubscirb.Mail = mail;
                    db.tbl_MailSubscriber.Add(mailsubscirb);
                    db.SaveChanges();
                    return Ok("You Successfully subscribe");
                }
                else
                {
                    return Ok("You already subscribe");
                }

            }
            catch (Exception e)
            {

                return Ok(e.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult ContactEmail(Contact cvm)
        {

            string emailContent = "<html><head><style>body{font-family:Tahoma;font-size:11px}</style></head><body><h3> Dear Samreen Saeed</h3><p> We have received your message below and we’ll be in touch with you very soon.</p><p> <b>Customer Name:</b> [customerName]<br><b>Customer Email: </b>[customerEmail]<br><b>Subject:</b> [emailSubject]<br> <b>Message:</b>[customerMessage]</p><p> Regards The GK Direct Team</p></body></html>";
            emailContent = emailContent.Replace("[customerName]", cvm.Name).Replace("[customerEmail]", cvm.Email).Replace("[emailSubject]", cvm.Subject).Replace("[customerMessage]", cvm.Message);

            this.SendEmail(cvm.Email, "Thank you for contacting us at GK.", emailContent, "");
            this.SendEmail("customerservice@gkdirect.co.uk", "Someone is Contacting Us at GK", emailContent, "");

            return Ok(new { status = true, Message = "Successfully create" });
        }

        protected bool SendEmail(string email, string subjec, string body, string name)
        {
            try
            {
                var senderEmail = ConfigurationManager.AppSettings["SenderEmail"];
                var senderPassword = ConfigurationManager.AppSettings["SenderPassword"];
                string senderMail = senderEmail;
                string senderPass = senderPassword;
                return EmailSender.SendEmail(senderEmail, senderPass, subjec, email, body);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

