﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Interfaces
{
    public interface IBookingPaymentService
    {
        decimal CustomerUserId();
        bool AddShippingDetails(tbl_gk_ShippingDetails shippingDetails);
        bool AddCheckOutDetails(tbl_gk_Checkout checkout);
        List<tbl_gk_Checkout> GetOrderHistory(string email);
    }
}
