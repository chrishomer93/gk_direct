﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using GkTiesBackend.ViewModels;

namespace BusinessEntityManager.Interfaces
{
  public  interface ICustomerService
    {
        List<tbl_gk_User> loadCustomer();
        bool SaveCustomer(CustomerViewModel cvm);
        bool DeleteCustomer(decimal Id);
        List<tbl_gk_Category> LoadCategory();
    }
}
