﻿using BusinessEntityManager.Interfaces;
using GkTiesBackend.ViewModels;
using System.Web.Mvc;


namespace GkTiesBackend.Controllers
{
    public class AdminController : Controller
    {
        private IAdminService adminService;

        public AdminController()
        {

        }

        public AdminController(IAdminService _adminService)
        {
            this.adminService = _adminService;
        }

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(AdminLoginViewModel am)
        {
            if (ModelState.IsValid)
            {
                var findLogin = this.adminService.LoginAdmin(am.Email, am.Passwrod);
                if (findLogin != null)
                {
                    Session["AdminLogi"] = findLogin.Email;
                    return RedirectToAction("Index", "Home");
                }
            }

            return View();
        }
        public ActionResult Logout()
        {
            Session.Remove("AdminLogi");
            return RedirectToAction("Index", "Admin");
        }

    }
}