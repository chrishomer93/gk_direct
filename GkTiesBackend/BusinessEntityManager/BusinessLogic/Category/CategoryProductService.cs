﻿using BusinessEntityManager.Interfaces;
using Database;
using GkTiesBackend.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Classes
{
    public class CategoryProductService : ICategoryProductService
    {
        public List<tbl_gk_Product> GetAllProductByCategory(decimal catId)
        {
            return new ProductRepositroy().GetAll(x => x.fld_CategoryId == catId);
        }

        public List<tbl_gk_Product> GetAllNewArrival()
        {
            return new ProductRepositroy().GetAll(x => x.fld_Option == "New").ToList().OrderByDescending(x => x.Id).ToList();
        }

        public List<tbl_gk_Product> GetAllNewArrivalProduct()
        {
            return new ProductRepositroy().GetAll(x => x.fld_Option == "New").ToList().OrderByDescending(x => x.Id).Take(4).ToList();
        }

        public List<tbl_gk_Product> GetAllProduct()
        {
            return new ProductRepositroy().GetAll().ToList();
        }

        public List<tbl_gk_Product> GetNewProductArrival(decimal id)
        {
            return new ProductRepositroy().GetAll(x => x.fld_CategoryId == id).OrderByDescending(x => x.Id).Take(4).ToList();
        }

        public List<tbl_gk_Product> GetProductByColor(decimal id, string color)
        {
            return new ProductRepositroy().GetAll(x => x.fld_CategoryId == id && x.fld_Color == color);
        }

        public List<tbl_gk_Product> GetProductByMaterial(decimal id, string material)
        {
            return new ProductRepositroy().GetAll(x => x.fld_CategoryId == id && x.fld_Material == material);
        }

        public List<tbl_gk_Product> GetProductByPattern(decimal id, string pattern)
        {
            return new ProductRepositroy().GetAll(x => x.fld_CategoryId == id && x.fld_Pattern == pattern);
        }

        public List<tbl_gk_Product> GetProductByPrice(decimal id, decimal price)
        {
            return new ProductRepositroy().GetAll(x => x.fld_CategoryId == id && x.fld_Price < price);
        }

        public List<tbl_gk_Product> GetProductByStyle(decimal id, string style)
        {
            return new ProductRepositroy().GetAll(x => x.fld_CategoryId == id && x.fld_Style == style);
        }

        public List<tbl_gk_Product> GetProductByWidth(decimal id, string width)
        {
            return new ProductRepositroy().GetAll(x => x.fld_CategoryId == id && x.fld_Width == width);
        }

        public tbl_gk_Product GetProductDetail(decimal id)
        {
            return new ProductRepositroy().FirstOrDefault(x => x.Id == id);
        }

        public int FindCategoryCountDetailsProductStyle(string tieStyle, decimal catId)
        {
            var AllProduct = this.GetAllProductByCategory(catId);
            return AllProduct.Where(x => x.fld_Style == tieStyle).ToList().Count;
        }

        public int FindCategoryCountDetailsProductPattern(string pattern, decimal catId)
        {
            var AllProduct = this.GetAllProductByCategory(catId);
            return AllProduct.Where(x => x.fld_Pattern == pattern).ToList().Count;
        }

        public int FindCategoryCountDetailsProductWidth(string width, decimal catId)
        {
            var AllProduct = this.GetAllProductByCategory(catId);
            return AllProduct.Where(x => x.fld_Width == width).ToList().Count;
        }

        public int FindCategoryCountDetailsProductMaterial(string material, decimal catId)
        {
            var AllProduct = this.GetAllProductByCategory(catId);
            return AllProduct.Where(x => x.fld_Material == material).ToList().Count;
        }
    }
}
