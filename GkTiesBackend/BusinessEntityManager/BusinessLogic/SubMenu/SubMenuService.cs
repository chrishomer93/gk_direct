﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Database;
using BusinessEntityManager.Interfaces;
using System.Data.Entity;

namespace BusinessEntityManager.Classes
{
    public class SubMenuService : ISubMenuService
    {
        public tbl_SubMenu Create(tbl_SubMenu tbl_SubMenu)
        {
            using (Gk_TiesEntities1 db= new Gk_TiesEntities1())
            {
              var result=  db.tbl_SubMenu.Add(tbl_SubMenu);
                db.SaveChanges();
                return result;
            }         
        }

        public tbl_SubMenu Delete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_SubMenu tbl_SubMenu = db.tbl_SubMenu.Find(id);
                db.tbl_SubMenu.Remove(tbl_SubMenu);
                db.SaveChanges();
                return tbl_SubMenu;
            }
        }

        public tbl_SubMenu Details(decimal id)
        {
            using (Gk_TiesEntities1 db=new Gk_TiesEntities1())
            {
                tbl_SubMenu tbl_SubMenu = db.tbl_SubMenu.Find(id);
                return tbl_SubMenu;
            }
        }

        public tbl_SubMenu getDelete(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_SubMenu tbl_SubMenu = db.tbl_SubMenu.Find(id);
                return tbl_SubMenu;
            }
        }

        public tbl_SubMenu getEdit(decimal id)
        {
            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
                tbl_SubMenu tbl_SubMenu = db.tbl_SubMenu.Find(id);
                return tbl_SubMenu;
            }
        }

        public tbl_SubMenu postEdit(tbl_SubMenu tbl_SubMenu)
        {

            using (Gk_TiesEntities1 db = new Gk_TiesEntities1())
            {
              db.Entry(tbl_SubMenu).State = EntityState.Modified;
                db.SaveChanges();
                return tbl_SubMenu;
            }

        }
    }
}
