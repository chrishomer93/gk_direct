﻿using EmailHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GkTiesBackend.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmailController : ApiController
    {
        public EmailController()
        {

        }

        [HttpPost]
        [ActionName("SendNewsLetterEmail")]
        public IHttpActionResult SendNewsLetterEmail(string email)
        {
            try
            {
                var receiverEmail = ConfigurationManager.AppSettings["SupportEmail"];
                var senderEmail = ConfigurationManager.AppSettings["SenderEmail"];
                var senderPassword = ConfigurationManager.AppSettings["SenderPassword"];
                string senderMail = senderEmail;
                string senderPass = senderPassword;

                string body = "You subscribed to our newsletter. you will recieve regular emails from us now";

                EmailSender.SendEmail(senderEmail, senderPass, "Welcome to GkTies", receiverEmail, body);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(true);
        }
    }
}
