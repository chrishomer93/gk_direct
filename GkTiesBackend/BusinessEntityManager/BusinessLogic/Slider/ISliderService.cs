﻿using Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntityManager.Interfaces
{
    public interface ISliderService
    {
        List<tbl_Slider> GetSlider();
        List<tbl_Slider> GetAllSlider();
        bool AddSlider(tbl_Slider slider);
        bool UpdateSlider(tbl_Slider slider);
        bool DeleteSlider(decimal sliderId);
        tbl_Slider FindSlider(decimal sliderId);
    }
}
